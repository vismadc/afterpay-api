# Visma\AfterPayApi\CustomerAccountApi

All URIs are relative to *https://ap-live-devportal-sandbox.azurewebsites.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerAccountAddEvent**](CustomerAccountApi.md#customerAccountAddEvent) | **POST** /api/v3/customer-account/{accountNumber}/event | Add event to account
[**customerAccountAddTransaction**](CustomerAccountApi.md#customerAccountAddTransaction) | **POST** /api/v3/customer-account/{accountNumber}/transaction | Add transaction to account
[**customerAccountApplyForCredit**](CustomerAccountApi.md#customerAccountApplyForCredit) | **POST** /api/v3/customer-account | Apply for customer account credit
[**customerAccountCreateUser**](CustomerAccountApi.md#customerAccountCreateUser) | **POST** /api/v3/customer-account/{accountNumber}/user | Add user to account
[**customerAccountCreditLimit**](CustomerAccountApi.md#customerAccountCreditLimit) | **GET** /api/v3/customer-account/{accountNumber}/credit-limit | Get remaining account credit limit
[**customerAccountDeleteTransaction**](CustomerAccountApi.md#customerAccountDeleteTransaction) | **DELETE** /api/v3/customer-account/{accountNumber}/transaction/{transactionId} | Cancels transaction made by account
[**customerAccountLookupBySSN**](CustomerAccountApi.md#customerAccountLookupBySSN) | **GET** /api/v3/customer-account/{ssn} | Find customer account by SSN (social security number)


# **customerAccountAddEvent**
> \Visma\AfterPayApi\Model\AddEventToAccountResponse customerAccountAddEvent($accountNumber, $request)

Add event to account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CustomerAccountApi();
$accountNumber = "accountNumber_example"; // string | Account number
$request = new \Visma\AfterPayApi\Model\AddEventToAccountRequest(); // \Visma\AfterPayApi\Model\AddEventToAccountRequest | 

try {
    $result = $api_instance->customerAccountAddEvent($accountNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountAddEvent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountNumber** | **string**| Account number |
 **request** | [**\Visma\AfterPayApi\Model\AddEventToAccountRequest**](../Model/AddEventToAccountRequest.md)|  |

### Return type

[**\Visma\AfterPayApi\Model\AddEventToAccountResponse**](../Model/AddEventToAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountAddTransaction**
> \Visma\AfterPayApi\Model\AddTransactionResponse customerAccountAddTransaction($accountNumber, $request)

Add transaction to account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CustomerAccountApi();
$accountNumber = "accountNumber_example"; // string | Account number
$request = new \Visma\AfterPayApi\Model\AddTransactionRequest(); // \Visma\AfterPayApi\Model\AddTransactionRequest | Request

try {
    $result = $api_instance->customerAccountAddTransaction($accountNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountAddTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountNumber** | **string**| Account number |
 **request** | [**\Visma\AfterPayApi\Model\AddTransactionRequest**](../Model/AddTransactionRequest.md)| Request |

### Return type

[**\Visma\AfterPayApi\Model\AddTransactionResponse**](../Model/AddTransactionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountApplyForCredit**
> \Visma\AfterPayApi\Model\ApplyForAccountCreditResponse customerAccountApplyForCredit($request)

Apply for customer account credit

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CustomerAccountApi();
$request = new \Visma\AfterPayApi\Model\ApplyForAccountCreditRequest(); // \Visma\AfterPayApi\Model\ApplyForAccountCreditRequest | 

try {
    $result = $api_instance->customerAccountApplyForCredit($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountApplyForCredit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Visma\AfterPayApi\Model\ApplyForAccountCreditRequest**](../Model/ApplyForAccountCreditRequest.md)|  |

### Return type

[**\Visma\AfterPayApi\Model\ApplyForAccountCreditResponse**](../Model/ApplyForAccountCreditResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountCreateUser**
> \Visma\AfterPayApi\Model\CreateUserResponse customerAccountCreateUser($accountNumber, $request)

Add user to account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CustomerAccountApi();
$accountNumber = "accountNumber_example"; // string | Account number
$request = new \Visma\AfterPayApi\Model\CreateUserRequest(); // \Visma\AfterPayApi\Model\CreateUserRequest | 

try {
    $result = $api_instance->customerAccountCreateUser($accountNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountCreateUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountNumber** | **string**| Account number |
 **request** | [**\Visma\AfterPayApi\Model\CreateUserRequest**](../Model/CreateUserRequest.md)|  |

### Return type

[**\Visma\AfterPayApi\Model\CreateUserResponse**](../Model/CreateUserResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountCreditLimit**
> \Visma\AfterPayApi\Model\GetRemainingLimitResponse customerAccountCreditLimit($accountNumber)

Get remaining account credit limit

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CustomerAccountApi();
$accountNumber = "accountNumber_example"; // string | Account number

try {
    $result = $api_instance->customerAccountCreditLimit($accountNumber);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountCreditLimit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountNumber** | **string**| Account number |

### Return type

[**\Visma\AfterPayApi\Model\GetRemainingLimitResponse**](../Model/GetRemainingLimitResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountDeleteTransaction**
> \Visma\AfterPayApi\Model\DeleteTransactionResponse customerAccountDeleteTransaction($accountNumber, $transactionId)

Cancels transaction made by account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CustomerAccountApi();
$accountNumber = "accountNumber_example"; // string | Account number
$transactionId = "transactionId_example"; // string | Transaction ID

try {
    $result = $api_instance->customerAccountDeleteTransaction($accountNumber, $transactionId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountDeleteTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountNumber** | **string**| Account number |
 **transactionId** | **string**| Transaction ID |

### Return type

[**\Visma\AfterPayApi\Model\DeleteTransactionResponse**](../Model/DeleteTransactionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountLookupBySSN**
> \Visma\AfterPayApi\Model\LookupAccountBySsnResponse customerAccountLookupBySSN($ssn)

Find customer account by SSN (social security number)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CustomerAccountApi();
$ssn = "ssn_example"; // string | 

try {
    $result = $api_instance->customerAccountLookupBySSN($ssn);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountLookupBySSN: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ssn** | **string**|  |

### Return type

[**\Visma\AfterPayApi\Model\LookupAccountBySsnResponse**](../Model/LookupAccountBySsnResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

