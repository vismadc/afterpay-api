# Visma\AfterPayApi\CommonApi

All URIs are relative to *https://ap-live-devportal-sandbox.azurewebsites.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**commonCustomerLookup**](CommonApi.md#commonCustomerLookup) | **POST** /api/v3/lookup/customer | Returns address based on input (Social security number, mobile number, Organization number).
[**commonGetVersion**](CommonApi.md#commonGetVersion) | **GET** /api/v3/version | Gets the version of the service
[**commonUpdateCustomer**](CommonApi.md#commonUpdateCustomer) | **PATCH** /api/v3/customers/{customerNumber}/updateCustomer | Updates customer information
[**commonValidateAddress**](CommonApi.md#commonValidateAddress) | **POST** /api/v3/validate/address | Check of the delivered customer addresses as well as a phonetic and associative identification of duplicates.  Additionally, checks of client specific negative or positive lists can be processed. Usually, the AddressCheck is  used for the pure verification of the address data e.g. for registration processes.
[**commonValidateBankAccount**](CommonApi.md#commonValidateBankAccount) | **POST** /api/v3/validate/bank-account | Validates and evaluates the account and bank details in the context of direct debit payment.  It is possible to transfer either the combination of BankCode and AccountNumber or IBAN and BIC
[**commonVirtualBankAccount**](CommonApi.md#commonVirtualBankAccount) | **GET** /api/v3/lookup/virtual-bank-account/order/{orderNumber} | Returns the virtual bank account for a specified customer


# **commonCustomerLookup**
> \Visma\AfterPayApi\Model\CustomerLookupResponse commonCustomerLookup($request)

Returns address based on input (Social security number, mobile number, Organization number).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CommonApi();
$request = new \Visma\AfterPayApi\Model\CustomerLookupRequest(); // \Visma\AfterPayApi\Model\CustomerLookupRequest | 

try {
    $result = $api_instance->commonCustomerLookup($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonCustomerLookup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Visma\AfterPayApi\Model\CustomerLookupRequest**](../Model/CustomerLookupRequest.md)|  |

### Return type

[**\Visma\AfterPayApi\Model\CustomerLookupResponse**](../Model/CustomerLookupResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonGetVersion**
> \Visma\AfterPayApi\Model\GetVersionResponse commonGetVersion()

Gets the version of the service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CommonApi();

try {
    $result = $api_instance->commonGetVersion();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonGetVersion: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Visma\AfterPayApi\Model\GetVersionResponse**](../Model/GetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonUpdateCustomer**
> \Visma\AfterPayApi\Model\UpdateCustomerResponse commonUpdateCustomer($customerNumber, $request)

Updates customer information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CommonApi();
$customerNumber = "customerNumber_example"; // string | 
$request = new \Visma\AfterPayApi\Model\UpdateCustomerRequest(); // \Visma\AfterPayApi\Model\UpdateCustomerRequest | 

try {
    $result = $api_instance->commonUpdateCustomer($customerNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonUpdateCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerNumber** | **string**|  |
 **request** | [**\Visma\AfterPayApi\Model\UpdateCustomerRequest**](../Model/UpdateCustomerRequest.md)|  |

### Return type

[**\Visma\AfterPayApi\Model\UpdateCustomerResponse**](../Model/UpdateCustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonValidateAddress**
> \Visma\AfterPayApi\Model\ValidateAddressResponse commonValidateAddress($request)

Check of the delivered customer addresses as well as a phonetic and associative identification of duplicates.  Additionally, checks of client specific negative or positive lists can be processed. Usually, the AddressCheck is  used for the pure verification of the address data e.g. for registration processes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CommonApi();
$request = new \Visma\AfterPayApi\Model\ValidateAddressRequest(); // \Visma\AfterPayApi\Model\ValidateAddressRequest | 

try {
    $result = $api_instance->commonValidateAddress($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonValidateAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Visma\AfterPayApi\Model\ValidateAddressRequest**](../Model/ValidateAddressRequest.md)|  |

### Return type

[**\Visma\AfterPayApi\Model\ValidateAddressResponse**](../Model/ValidateAddressResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonValidateBankAccount**
> \Visma\AfterPayApi\Model\ValidateBankAccountResponse commonValidateBankAccount($request)

Validates and evaluates the account and bank details in the context of direct debit payment.  It is possible to transfer either the combination of BankCode and AccountNumber or IBAN and BIC

API call will validate given bank account is passing account format rules.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CommonApi();
$request = new \Visma\AfterPayApi\Model\ValidateBankAccountRequest(); // \Visma\AfterPayApi\Model\ValidateBankAccountRequest | 

try {
    $result = $api_instance->commonValidateBankAccount($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonValidateBankAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Visma\AfterPayApi\Model\ValidateBankAccountRequest**](../Model/ValidateBankAccountRequest.md)|  |

### Return type

[**\Visma\AfterPayApi\Model\ValidateBankAccountResponse**](../Model/ValidateBankAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonVirtualBankAccount**
> \Visma\AfterPayApi\Model\GetVirtualBankAccountResponse commonVirtualBankAccount($orderNumber)

Returns the virtual bank account for a specified customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\CommonApi();
$orderNumber = "orderNumber_example"; // string | 

try {
    $result = $api_instance->commonVirtualBankAccount($orderNumber);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonVirtualBankAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**|  |

### Return type

[**\Visma\AfterPayApi\Model\GetVirtualBankAccountResponse**](../Model/GetVirtualBankAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

