# Visma\AfterPayApi\ProductApi

All URIs are relative to *https://ap-live-devportal-sandbox.azurewebsites.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productAvailableInstallmentPlans**](ProductApi.md#productAvailableInstallmentPlans) | **POST** /api/v3/lookup/installment-plans | Returns the available installment plans for the specific product/basket value. Returns monthly installment amount, interest and fees. Typically used on a product page.


# **productAvailableInstallmentPlans**
> \Visma\AfterPayApi\Model\AvailableInstallmentPlansResponse productAvailableInstallmentPlans($request)

Returns the available installment plans for the specific product/basket value. Returns monthly installment amount, interest and fees. Typically used on a product page.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\ProductApi();
$request = new \Visma\AfterPayApi\Model\AvailableInstallmentPlansRequest(); // \Visma\AfterPayApi\Model\AvailableInstallmentPlansRequest | 

try {
    $result = $api_instance->productAvailableInstallmentPlans($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productAvailableInstallmentPlans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Visma\AfterPayApi\Model\AvailableInstallmentPlansRequest**](../Model/AvailableInstallmentPlansRequest.md)|  |

### Return type

[**\Visma\AfterPayApi\Model\AvailableInstallmentPlansResponse**](../Model/AvailableInstallmentPlansResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

