# Visma\AfterPayApi\OrderManagementApi

All URIs are relative to *https://ap-live-devportal-sandbox.azurewebsites.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orderManagementAddShippingDetails**](OrderManagementApi.md#orderManagementAddShippingDetails) | **POST** /api/v3/orders/{orderNumber}/captures/{captureNumber}/shipping-details | Add new shipping details information to the capture.
[**orderManagementCapture**](OrderManagementApi.md#orderManagementCapture) | **POST** /api/v3/orders/{orderNumber}/captures | Completes the payment that has been authorized. Typically done when the order is shipped. Can be a full or partial capture of the order amount.
[**orderManagementDeleteShippingDetails**](OrderManagementApi.md#orderManagementDeleteShippingDetails) | **DELETE** /api/v3/orders/{orderNumber}/captures/{captureNumber}/shipping-details/{shippingNumber} | Delete shipping details information from the specific capture.
[**orderManagementGetCapture**](OrderManagementApi.md#orderManagementGetCapture) | **GET** /api/v3/orders/{orderNumber}/captures/{captureNumber} | Returns all or specific captured payments of the order
[**orderManagementGetOrder**](OrderManagementApi.md#orderManagementGetOrder) | **GET** /api/v3/orders/{orderNumber} | Returns the contents of the specified order
[**orderManagementGetRefund**](OrderManagementApi.md#orderManagementGetRefund) | **GET** /api/v3/orders/{orderNumber}/refunds/{refundNumber} | Returns all or specific refunds of the order.
[**orderManagementGetReservation**](OrderManagementApi.md#orderManagementGetReservation) | **GET** /api/v3/checkout/pendingReservation/{orderHandle} | Returns the contents of the pending reservation
[**orderManagementGetShippingDetails**](OrderManagementApi.md#orderManagementGetShippingDetails) | **GET** /api/v3/orders/{orderNumber}/captures/{captureNumber}/shipping-details/{shippingNumber} | Returns all or specific shipping details information of the capture.
[**orderManagementGetVoid**](OrderManagementApi.md#orderManagementGetVoid) | **GET** /api/v3/orders/{orderNumber}/voids/{voidNumber} | Returns all or specific voided (cancelled) authorizations of the order.
[**orderManagementRefund**](OrderManagementApi.md#orderManagementRefund) | **POST** /api/v3/orders/{orderNumber}/refunds | Refunds a partially or fully captured payment.
[**orderManagementSearchOrder**](OrderManagementApi.md#orderManagementSearchOrder) | **POST** /api/v3/orders/search | Search order
[**orderManagementUpdateOrder**](OrderManagementApi.md#orderManagementUpdateOrder) | **POST** /api/v3/orders/{orderNumber}/updateOrder | Change the contents of an Authorized but not Captured order. Use this call to add or remove items in an existing Order, without creating a new orderNumber.&lt;br /&gt;  You must provide the entire body of the query, in the same format as Authorize, including all Order Items.  (If you added an item, Update Order must contain previously Authorized items as well. If you removed an item, Update Order must contain the new full list of Order Items.)&lt;br /&gt;  If the TotalGrossAmount has increased compared to the Authorize call for this orderNumber, a new Risk Check will be made for the additional amount.  If the additional Risk Check returns a rejection, the original Authorize will remain valid.
[**orderManagementUpdateShippingDetails**](OrderManagementApi.md#orderManagementUpdateShippingDetails) | **PATCH** /api/v3/orders/{orderNumber}/captures/{captureNumber}/shipping-details/{shippingNumber} | Update shipping details information of the specific capture.
[**orderManagementVoid**](OrderManagementApi.md#orderManagementVoid) | **POST** /api/v3/orders/{orderNumber}/voids | Void (cancel) an authorization that has not been captured.


# **orderManagementAddShippingDetails**
> \Visma\AfterPayApi\Model\AddShippingDetailsResponse orderManagementAddShippingDetails($orderNumber, $captureNumber, $request)

Add new shipping details information to the capture.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$captureNumber = "captureNumber_example"; // string | Capture number
$request = new \Visma\AfterPayApi\Model\AddShippingDetailsRequest(); // \Visma\AfterPayApi\Model\AddShippingDetailsRequest | Request object

try {
    $result = $api_instance->orderManagementAddShippingDetails($orderNumber, $captureNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementAddShippingDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **captureNumber** | **string**| Capture number |
 **request** | [**\Visma\AfterPayApi\Model\AddShippingDetailsRequest**](../Model/AddShippingDetailsRequest.md)| Request object |

### Return type

[**\Visma\AfterPayApi\Model\AddShippingDetailsResponse**](../Model/AddShippingDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementCapture**
> \Visma\AfterPayApi\Model\CaptureResponse orderManagementCapture($orderNumber, $request)

Completes the payment that has been authorized. Typically done when the order is shipped. Can be a full or partial capture of the order amount.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$request = new \Visma\AfterPayApi\Model\CaptureRequest(); // \Visma\AfterPayApi\Model\CaptureRequest | Request object

try {
    $result = $api_instance->orderManagementCapture($orderNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementCapture: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **request** | [**\Visma\AfterPayApi\Model\CaptureRequest**](../Model/CaptureRequest.md)| Request object | [optional]

### Return type

[**\Visma\AfterPayApi\Model\CaptureResponse**](../Model/CaptureResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementDeleteShippingDetails**
> \Visma\AfterPayApi\Model\DeleteShippingDetailsResponse orderManagementDeleteShippingDetails($orderNumber, $captureNumber, $shippingNumber)

Delete shipping details information from the specific capture.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$captureNumber = "captureNumber_example"; // string | Capture number
$shippingNumber = 56; // int | Shipping number

try {
    $result = $api_instance->orderManagementDeleteShippingDetails($orderNumber, $captureNumber, $shippingNumber);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementDeleteShippingDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **captureNumber** | **string**| Capture number |
 **shippingNumber** | **int**| Shipping number |

### Return type

[**\Visma\AfterPayApi\Model\DeleteShippingDetailsResponse**](../Model/DeleteShippingDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetCapture**
> \Visma\AfterPayApi\Model\GetAllCapturesResponse orderManagementGetCapture($orderNumber, $captureNumber)

Returns all or specific captured payments of the order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$captureNumber = "captureNumber_example"; // string | Capture number

try {
    $result = $api_instance->orderManagementGetCapture($orderNumber, $captureNumber);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetCapture: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **captureNumber** | **string**| Capture number |

### Return type

[**\Visma\AfterPayApi\Model\GetAllCapturesResponse**](../Model/GetAllCapturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetOrder**
> \Visma\AfterPayApi\Model\GetOrderResponse orderManagementGetOrder($orderNumber)

Returns the contents of the specified order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number

try {
    $result = $api_instance->orderManagementGetOrder($orderNumber);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |

### Return type

[**\Visma\AfterPayApi\Model\GetOrderResponse**](../Model/GetOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetRefund**
> \Visma\AfterPayApi\Model\GetAllRefundsResponse orderManagementGetRefund($orderNumber, $refundNumber)

Returns all or specific refunds of the order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$refundNumber = "refundNumber_example"; // string | Refund number

try {
    $result = $api_instance->orderManagementGetRefund($orderNumber, $refundNumber);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **refundNumber** | **string**| Refund number |

### Return type

[**\Visma\AfterPayApi\Model\GetAllRefundsResponse**](../Model/GetAllRefundsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetReservation**
> \Visma\AfterPayApi\Model\GetReservationResponse orderManagementGetReservation($orderHandle)

Returns the contents of the pending reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderHandle = "orderHandle_example"; // string | Order handle

try {
    $result = $api_instance->orderManagementGetReservation($orderHandle);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderHandle** | **string**| Order handle |

### Return type

[**\Visma\AfterPayApi\Model\GetReservationResponse**](../Model/GetReservationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetShippingDetails**
> \Visma\AfterPayApi\Model\GetAllShippingDetailsResponse orderManagementGetShippingDetails($orderNumber, $captureNumber, $shippingNumber)

Returns all or specific shipping details information of the capture.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$captureNumber = "captureNumber_example"; // string | Capture number
$shippingNumber = 56; // int | Shipping number

try {
    $result = $api_instance->orderManagementGetShippingDetails($orderNumber, $captureNumber, $shippingNumber);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetShippingDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **captureNumber** | **string**| Capture number |
 **shippingNumber** | **int**| Shipping number |

### Return type

[**\Visma\AfterPayApi\Model\GetAllShippingDetailsResponse**](../Model/GetAllShippingDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetVoid**
> \Visma\AfterPayApi\Model\GetVoidsResponse orderManagementGetVoid($orderNumber, $voidNumber)

Returns all or specific voided (cancelled) authorizations of the order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$voidNumber = "voidNumber_example"; // string | Void number

try {
    $result = $api_instance->orderManagementGetVoid($orderNumber, $voidNumber);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetVoid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **voidNumber** | **string**| Void number |

### Return type

[**\Visma\AfterPayApi\Model\GetVoidsResponse**](../Model/GetVoidsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementRefund**
> \Visma\AfterPayApi\Model\RefundOrderResponse orderManagementRefund($orderNumber, $request)

Refunds a partially or fully captured payment.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$request = new \Visma\AfterPayApi\Model\RefundOrderRequest(); // \Visma\AfterPayApi\Model\RefundOrderRequest | Request object

try {
    $result = $api_instance->orderManagementRefund($orderNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **request** | [**\Visma\AfterPayApi\Model\RefundOrderRequest**](../Model/RefundOrderRequest.md)| Request object | [optional]

### Return type

[**\Visma\AfterPayApi\Model\RefundOrderResponse**](../Model/RefundOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementSearchOrder**
> \Visma\AfterPayApi\Model\SearchOrderResponse orderManagementSearchOrder($request)

Search order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$request = new \Visma\AfterPayApi\Model\SearchOrderRequest(); // \Visma\AfterPayApi\Model\SearchOrderRequest | Request object

try {
    $result = $api_instance->orderManagementSearchOrder($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementSearchOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Visma\AfterPayApi\Model\SearchOrderRequest**](../Model/SearchOrderRequest.md)| Request object |

### Return type

[**\Visma\AfterPayApi\Model\SearchOrderResponse**](../Model/SearchOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementUpdateOrder**
> \Visma\AfterPayApi\Model\UpdateOrderResponse orderManagementUpdateOrder($orderNumber, $request)

Change the contents of an Authorized but not Captured order. Use this call to add or remove items in an existing Order, without creating a new orderNumber.<br />  You must provide the entire body of the query, in the same format as Authorize, including all Order Items.  (If you added an item, Update Order must contain previously Authorized items as well. If you removed an item, Update Order must contain the new full list of Order Items.)<br />  If the TotalGrossAmount has increased compared to the Authorize call for this orderNumber, a new Risk Check will be made for the additional amount.  If the additional Risk Check returns a rejection, the original Authorize will remain valid.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$request = new \Visma\AfterPayApi\Model\UpdateOrderRequest(); // \Visma\AfterPayApi\Model\UpdateOrderRequest | Request object

try {
    $result = $api_instance->orderManagementUpdateOrder($orderNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementUpdateOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **request** | [**\Visma\AfterPayApi\Model\UpdateOrderRequest**](../Model/UpdateOrderRequest.md)| Request object |

### Return type

[**\Visma\AfterPayApi\Model\UpdateOrderResponse**](../Model/UpdateOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementUpdateShippingDetails**
> \Visma\AfterPayApi\Model\UpdateShippingDetailsResponse orderManagementUpdateShippingDetails($orderNumber, $captureNumber, $shippingNumber, $request)

Update shipping details information of the specific capture.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$captureNumber = "captureNumber_example"; // string | Capture number
$shippingNumber = 56; // int | Shipping number
$request = new \Visma\AfterPayApi\Model\UpdateShippingDetailsRequest(); // \Visma\AfterPayApi\Model\UpdateShippingDetailsRequest | Request object

try {
    $result = $api_instance->orderManagementUpdateShippingDetails($orderNumber, $captureNumber, $shippingNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementUpdateShippingDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **captureNumber** | **string**| Capture number |
 **shippingNumber** | **int**| Shipping number |
 **request** | [**\Visma\AfterPayApi\Model\UpdateShippingDetailsRequest**](../Model/UpdateShippingDetailsRequest.md)| Request object |

### Return type

[**\Visma\AfterPayApi\Model\UpdateShippingDetailsResponse**](../Model/UpdateShippingDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementVoid**
> \Visma\AfterPayApi\Model\VoidAuthorizationResponse orderManagementVoid($orderNumber, $request)

Void (cancel) an authorization that has not been captured.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Visma\AfterPayApi\Api\OrderManagementApi();
$orderNumber = "orderNumber_example"; // string | Order number
$request = new \Visma\AfterPayApi\Model\VoidAuthorizationRequest(); // \Visma\AfterPayApi\Model\VoidAuthorizationRequest | Request

try {
    $result = $api_instance->orderManagementVoid($orderNumber, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementVoid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **string**| Order number |
 **request** | [**\Visma\AfterPayApi\Model\VoidAuthorizationRequest**](../Model/VoidAuthorizationRequest.md)| Request | [optional]

### Return type

[**\Visma\AfterPayApi\Model\VoidAuthorizationResponse**](../Model/VoidAuthorizationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

