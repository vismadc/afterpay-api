# ConsolidatedInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoiceDate** | [**\DateTime**](\DateTime.md) | Date when invoice will be created | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


