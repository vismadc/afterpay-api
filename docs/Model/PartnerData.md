# PartnerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pspName** | **string** | Name of the PSP | [optional] 
**pspType** | **string** | Type of the PSP | [optional] 
**trackingProvider** | **string** | If device fingerprinting is applied, please specify the name of the provider | [optional] 
**trackingSessionId** | **string** | Session id of tracking script | [optional] 
**trackingScore** | **string** | Score provided by tracking provider | [optional] 
**challengedScore** | **string** | Score provided by partner if transaction is challenged | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


