# CreateUserRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyId** | **int** | Company ID | [optional] 
**customer** | [**\Visma\AfterPayApi\Model\ParkCustomer**](ParkCustomer.md) | Park customer details | 
**limit** | **double** | User&#39;s limit in park | 
**nfcId** | **string** | ID of NFC chip linked with this user | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


