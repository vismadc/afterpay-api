# GetAllShippingDetailsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shippingDetails** | [**\Visma\AfterPayApi\Model\ShippingDetailsWithNumber[]**](ShippingDetailsWithNumber.md) | Collection of shipping details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


