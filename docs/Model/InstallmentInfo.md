# InstallmentInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basketAmount** | **double** | Basket amount | [optional] 
**numberOfInstallments** | **int** | Number of installments | [optional] 
**installmentAmount** | **double** | Monthly installment amount | [optional] 
**firstInstallmentAmount** | **double** | Amount of the first installment payment | [optional] 
**lastInstallmentAmount** | **double** | Amount of the last installment payment | [optional] 
**interestRate** | **double** | Interest rate | [optional] 
**effectiveInterestRate** | **double** | Effective interest rate | [optional] 
**effectiveAnnualPercentageRate** | **double** | Effective annual percentage rate | [optional] 
**totalInterestAmount** | **double** | Total interest amount | [optional] 
**startupFee** | **double** | Fee for opening up an installment plan | [optional] 
**monthlyFee** | **double** | Monthly fee for the installment amount | [optional] 
**totalAmount** | **double** | Total amount | [optional] 
**installmentProfileNumber** | **int** | Installment profile number | [optional] 
**readMore** | **string** | More information on installment process | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


