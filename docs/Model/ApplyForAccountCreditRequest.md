# ApplyForAccountCreditRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountLimit** | **double** | Account limit | 
**customer** | [**\Visma\AfterPayApi\Model\Customer**](Customer.md) | Customer linked with this account | 
**firstNoticeDate** | [**\DateTime**](\DateTime.md) | First notice date | [optional] 
**countryCode** | **string** | Country code | 
**conversationLanguage** | **string** | Conversation language. &#39;SE&#39; and &#39;DK&#39; are obsolete values, and will be removed in the future. Use &#39;SV&#39; instead of &#39;SE&#39;, and &#39;DA&#39; instead of &#39;DK&#39;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


