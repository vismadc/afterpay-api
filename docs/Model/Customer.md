# Customer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**salutation** | **string** | Salutation. | [optional] 
**firstName** | **string** | First name | [optional] 
**lastName** | **string** | Last name | [optional] 
**customerNumber** | **string** | Customer number | [optional] 
**organizationPersonalNo** | **string** | Organization (if customer is organization) or personal (if customer is person) identification number | [optional] 
**category** | **string** | Customer category. | 
**currency** | **string** | Customer currency | 
**distributionBy** | **string** | Specifies who will distribute information (invoices, notices, etc.) to customer | [optional] 
**distributionType** | **string** | Specifies how information (invoices, notices, etc.) will be distributed to customer | [optional] 
**email** | **string** | Customer email | [optional] 
**address** | [**\Visma\AfterPayApi\Model\Address**](Address.md) | Customer address details | [optional] 
**phone** | **string** | Customer&#39;s primary phone | [optional] 
**directPhone** | **string** | Customer&#39;s direct phone number (land line) | [optional] 
**mobilePhone** | **string** | Customer&#39;s mobile phone | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


