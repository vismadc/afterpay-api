# Cancellations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cancellationNo** | **string** | Order number | [optional] 
**cancellationAmount** | **double** | Amount of the cancelled items | [optional] 
**cancellationItems** | [**\Visma\AfterPayApi\Model\CancellationItem[]**](CancellationItem.md) | The list of items to be cancelled | [optional] 
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


