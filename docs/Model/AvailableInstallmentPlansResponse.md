# AvailableInstallmentPlansResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**availableInstallmentPlans** | [**\Visma\AfterPayApi\Model\InstallmentInfo[]**](InstallmentInfo.md) | Available installment plans | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


