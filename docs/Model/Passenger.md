# Passenger

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Continuous numbering | [optional] 
**firstName** | **string** | First name | [optional] 
**lastName** | **string** | Last name | [optional] 
**salutation** | **string** | Salutation | [optional] 
**dateOfBirth** | [**\DateTime**](\DateTime.md) | Date birth of passenger | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


