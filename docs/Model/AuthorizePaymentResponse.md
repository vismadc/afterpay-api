# AuthorizePaymentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outcome** | **string** | Outcome | [optional] 
**customer** | [**\Visma\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Customer | [optional] 
**deliveryCustomer** | [**\Visma\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | To be used if the receiver of the order differs from the billing customer | [optional] 
**reservationId** | **string** |  | [optional] 
**checkoutId** | **string** |  | [optional] 
**secureLoginUrl** | **string** | Url where consumer can be redirected to perform strong authentication. Filled only in case the  risk check decides that purchase is not allowed without strong authentication.  Contact your Account Manager to enable the feature. | [optional] 
**riskCheckMessages** | [**\Visma\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 
**expirationDate** | [**\DateTime**](Date.md) | The date and time when this authorization will expire if you did not make a Capture call.  If the deadline has passed, you must make a new Authorize call before Capturing this order. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


