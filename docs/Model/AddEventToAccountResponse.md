# AddEventToAccountResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventAdded** | **bool** | Indicates whether event was added or not | [optional] 
**transactionId** | **string** |  | [optional] 
**outcome** | **string** | Outcome | [optional] 
**riskCheckMessages** | [**\Visma\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


