# GetOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderDetails** | [**\Visma\AfterPayApi\Model\ResponseOrderDetails**](ResponseOrderDetails.md) | Order details | [optional] 
**captures** | [**\Visma\AfterPayApi\Model\Capture[]**](Capture.md) | Collection of captures | [optional] 
**refunds** | [**\Visma\AfterPayApi\Model\Refund[]**](Refund.md) | Collection of refunds | [optional] 
**cancellations** | [**\Visma\AfterPayApi\Model\Cancellations[]**](Cancellations.md) | Cancellation info | [optional] 
**payment** | [**\Visma\AfterPayApi\Model\Payment**](Payment.md) | Payment details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


