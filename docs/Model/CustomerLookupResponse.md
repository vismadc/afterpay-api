# CustomerLookupResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userProfiles** | [**\Visma\AfterPayApi\Model\UserProfile[]**](UserProfile.md) | List of user profiles matching to lookup parameters | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


