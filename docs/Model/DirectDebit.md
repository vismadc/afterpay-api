# DirectDebit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bankCode** | **string** | Direct debit swift, BIC, SortCode | [optional] 
**bankAccount** | **string** | Direct debit bank account | 
**token** | **string** | Unique string provided by AfterPay if the merchant is using tokenization.  Contact your Key Account Manager for more details. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


