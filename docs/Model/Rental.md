# Rental

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **string** | Name of rental company | [optional] 
**pickupLocation** | [**\Visma\AfterPayApi\Model\Address**](Address.md) | Pickup location | [optional] 
**dropoffLocation** | [**\Visma\AfterPayApi\Model\Address**](Address.md) | Drop off location | [optional] 
**startDate** | [**\DateTime**](\DateTime.md) | Pick up date and time | [optional] 
**endDate** | [**\DateTime**](\DateTime.md) | End date and time | [optional] 
**drivers** | [**\Visma\AfterPayApi\Model\CheckoutCustomer[]**](CheckoutCustomer.md) | Information about drivers who have rented a car | [optional] 
**price** | **double** | Price of a rental car | [optional] 
**currency** | **string** | Currency | [optional] 
**bookingReference** | **string** | Booking reference | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


