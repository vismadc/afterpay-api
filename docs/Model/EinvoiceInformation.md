# EinvoiceInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference** | **string** | The customer&#39;s e-invoice reference | [optional] 
**referenceType** | **string** | Type of the refernce. | [optional] 
**party** | **string** | Reference to the invoice party that should be used. For Finland the max length is 35 chars. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


