# ApplyForAccountCreditResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**\Visma\AfterPayApi\Model\Account**](Account.md) | Created account details | [optional] 
**temporaryExternalProblem** | **bool** | Indicates whether temporary external problem has happened during account creation or not | [optional] 
**customer** | [**\Visma\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Customer details | [optional] 
**outcome** | **string** | Outcome | [optional] 
**riskCheckMessages** | [**\Visma\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


