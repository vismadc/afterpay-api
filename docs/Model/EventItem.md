# EventItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemId** | **string** | Item ID | 
**itemDescription** | **string** | Item description | 
**unitCode** | **string** | Unit code | 
**quantity** | **double** | Quantity | 
**unitPrice** | **double** | Unit price (gross amount) | 
**netAmount** | **double** | Net amount of one item | [optional] 
**taxAmount** | **double** | Tax amount of one item | [optional] 
**discount** | **double** | Total discount | [optional] 
**grossAmount** | **double** | Total gross amount (unit price * quantity - discount) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


