# ResponseOrderDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **string** | Order ID | [optional] 
**orderNumber** | **string** | Order number | [optional] 
**totalNetAmount** | **double** | Total net amount of order | [optional] 
**totalGrossAmount** | **double** | Total gross amount of order | [optional] 
**currency** | **string** | Order currency | [optional] 
**orderChannelType** | **string** | Order channel type | [optional] 
**orderDeliveryType** | **string** | Order delivery type | [optional] 
**hasSeparateDeliveryAddress** | **bool** | Indicates whether order has separate delivery address or not | [optional] 
**customer** | [**\Visma\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | Customer information | [optional] 
**insertedAt** | [**\DateTime**](\DateTime.md) | Indicates the creation of the specific element (e.g. orderDetails) | [optional] 
**updatedAt** | [**\DateTime**](\DateTime.md) | Indicates the time of the latest change of the element | [optional] 
**imageUrl** | **string** | URL for the image of this product. It will be turned into a thumbnail and displayed in MyAfterPay,  on the invoice line next to the order item. The linked image must be a rectangle or square, width between 100 pixels and 1280 pixels. | [optional] 
**merchantImageUrl** | **string** | Image URL for the merchants brand. This image is shown at the top of the order page in MyAfterPay | [optional] 
**googleAnalyticsUserId** | **string** | Google Analytics User ID | [optional] 
**orderItems** | [**\Visma\AfterPayApi\Model\OrderItemExtended[]**](OrderItemExtended.md) | Order items | [optional] 
**expirationDate** | [**\DateTime**](Date.md) | The date and time when this authorization will expire if you did not make a Capture call.  If the deadline has passed, you must make a new Authorize call before Capturing this order. | [optional] 
**status** | **string** | Order status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


