# LookupAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **string** |  | [optional] 
**street2** | **string** |  | [optional] 
**street3** | **string** |  | [optional] 
**street4** | **string** |  | [optional] 
**streetNumber** | **string** |  | [optional] 
**flatNo** | **string** |  | [optional] 
**entrance** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**postalCode** | **string** | postalCode will not be provided when doing lookup on Organization number | [optional] 
**country** | **string** |  | [optional] 
**countryCode** | **string** |  | [optional] 
**companyName** | **string** |  | [optional] 
**maskedStreet** | **string** |  | [optional] 
**maskedStreet2** | **string** |  | [optional] 
**maskedStreet3** | **string** |  | [optional] 
**maskedStreet4** | **string** |  | [optional] 
**maskedPostalCode** | **string** |  | [optional] 
**maskedCompanyName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


