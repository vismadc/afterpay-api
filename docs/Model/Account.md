# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountLimit** | **double** | Amount of granted account limit if account has limit assigned to it | [optional] 
**accountNumber** | **string** | Customeraccounts identifier | [optional] 
**accountProfile** | **string** | Identifier of a specific accountProfile | [optional] 
**accountStatus** | **string** | Account status | [optional] 
**balance** | **double** | Current ledger balance | [optional] 
**bankAccount** | **string** | Bank account, which is linked to this account | [optional] 
**customerId** | **string** | Customer ID, which is linked to this account | [optional] 
**createdAt** | [**\DateTime**](\DateTime.md) | Date and time when account was created | [optional] 
**isStopped** | **bool** | Indicates whether account is stopped or not | [optional] 
**ocr** | **string** | OCR (Optical Character Recognition) number bound to this account | [optional] 
**lastNoticeDate** | [**\DateTime**](\DateTime.md) | Date of last notice | [optional] 
**nextNoticeDate** | [**\DateTime**](\DateTime.md) | Date of next notice | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


