# UserProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **string** | First name | [optional] 
**lastName** | **string** | Last name | [optional] 
**mobileNumber** | **string** | Mobile number | [optional] 
**eMail** | **string** | Email address | [optional] 
**maskedFirstName** | **string** | Masked First name | [optional] 
**maskedLastName** | **string** | Masked Last name | [optional] 
**maskedMobileNumber** | **string** | Masked Mobile number | [optional] 
**maskedEmail** | **string** | Masked Email address | [optional] 
**languageCode** | **string** | Language code | [optional] 
**addressList** | [**\Visma\AfterPayApi\Model\LookupAddress[]**](LookupAddress.md) | Array of LookupAddress objects | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


