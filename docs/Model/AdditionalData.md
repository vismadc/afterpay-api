# AdditionalData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pluginProvider** | **string** | Plugin Provider Name | [optional] 
**pluginVersion** | **string** | Plugin Version | [optional] 
**shopUrl** | **string** | URL of the Webshop | [optional] 
**shopPlatform** | **string** | Name of the platform used for the Webshop | [optional] 
**shopPlatformVersion** | **string** | Version of the webshop platform | [optional] 
**marketplace** | [**\Visma\AfterPayApi\Model\Marketplace[]**](Marketplace.md) | Additional information for marketplace set-ups | [optional] 
**subscription** | [**\Visma\AfterPayApi\Model\Subscription**](Subscription.md) | Additional information for subscription businesses | [optional] 
**partnerData** | [**\Visma\AfterPayApi\Model\PartnerData**](PartnerData.md) | Additional data to be provided by PSP or platform provider | [optional] 
**additionalPaymentInfo** | **string** | Additional payment information | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


