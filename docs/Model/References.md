# References

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**yourReference** | **string** | Can be used by purchasing company if they would like to provide internal reference | [optional] 
**ourReference** | **string** | Can be used by selling company if they would like to provide specific reference for purchasing company | [optional] 
**merchantId** | **string** | Merchant ID | [optional] 
**contractDate** | [**\DateTime**](\DateTime.md) | Contract Date | [optional] 
**invoiceDate** | [**\DateTime**](\DateTime.md) | Invoice Date | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


