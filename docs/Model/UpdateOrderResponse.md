# UpdateOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outcome** | **string** | Outcome | [optional] 
**reservationId** | **string** | Reservation ID | [optional] 
**checkoutId** | **string** | Checkout ID | [optional] 
**riskCheckMessages** | [**\Visma\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 
**expirationDate** | [**\DateTime**](Date.md) | The date and time when this authorization will expire if you did not make a Capture call.  If the deadline has passed, you must make a new Authorize call before Capturing this order. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


