# CampaignInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaignNumber** | **int** | Campaign number | [optional] 
**campaignType** | **string** |  | [optional] 
**activeFrom** | [**\DateTime**](\DateTime.md) |  | [optional] 
**activeTo** | [**\DateTime**](\DateTime.md) |  | [optional] 
**dueDate** | [**\DateTime**](Date.md) |  | [optional] 
**consumerFeeAmount** | **double** |  | [optional] 
**paymentTerm** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


