# ValidateBankAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bankAccount** | **string** | Account number | 
**bankCode** | **string** | Account swift number | [optional] 
**bankNumber** | **string** | Bank and branch identifier | [optional] 
**createToken** | **bool** |  | [optional] 
**nonce** | **string** | Unique string provided by AfterPay if the merchant is using Hosted Fields. Contact your Key Account Manager for more details. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


