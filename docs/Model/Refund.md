# Refund

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservationId** | **string** | Reservation ID | [optional] 
**customerNumber** | **string** | Customer number | [optional] 
**refundNumber** | **string** | Refund number | [optional] 
**orderNumber** | **string** | Order number | [optional] 
**amount** | **double** | Refunded amount | [optional] 
**balance** | **double** | Remaining amount to be refunded | [optional] 
**currency** | **string** | Currency code | [optional] 
**insertedAt** | [**\DateTime**](\DateTime.md) | Indicates the Refund creation time | [optional] 
**updatedAt** | [**\DateTime**](\DateTime.md) | Indicates the time when the refund was last updated | [optional] 
**captureNumber** | **string** | Capture number | [optional] 
**refundItems** | [**\Visma\AfterPayApi\Model\RefundItem[]**](RefundItem.md) | Refund items | [optional] 
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 
**transactionReference** | **string** | Transaction-specific reference from and for the PSP&#39;s system | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


