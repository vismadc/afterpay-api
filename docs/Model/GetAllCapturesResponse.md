# GetAllCapturesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**captures** | [**\Visma\AfterPayApi\Model\Capture[]**](Capture.md) | Collection of captures | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


