# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactionId** | **int** |  | [optional] 
**amount** | **double** |  | [optional] 
**createdAt** | [**\DateTime**](\DateTime.md) |  | [optional] 
**transactionDate** | [**\DateTime**](\DateTime.md) |  | [optional] 
**transactionType** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


