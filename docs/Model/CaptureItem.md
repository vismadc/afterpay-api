# CaptureItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productId** | **string** | Merchant Product identification number | 
**groupId** | **string** | Item group ID. The group this item belongs to. Provided by the merchant | [optional] 
**description** | **string** | Product name. For example \&quot;Black music player 64GB\&quot;. Everything over 255 characters will be truncated | 
**type** | **string** | Order item type | [optional] 
**netUnitPrice** | **double** | Net unit price | 
**grossUnitPrice** | **double** | Gross price per item | 
**quantity** | **double** | Quantity. Use of integer is strongly proposed. If you want to use decimal, please contact your integration manager. | 
**unitCode** | **string** | Unit code (for example pieces, liters, kilograms, etc.) | [optional] 
**vatCategory** | **string** | VAT category | [optional] 
**vatPercent** | **double** | Tax percent | 
**vatAmount** | **double** | Tax amount per item | 
**imageUrl** | **string** | URL for the image of this product. It will be turned into a thumbnail and displayed in MyAfterPay, on the invoice line next to the order item.   The linked image must be a rectangle or square, width between 100 pixels and 1280 pixels. | [optional] 
**googleProductCategoryId** | **int** | Google product category ID | [optional] 
**googleProductCategory** | **string** | Indicates the category of the item based on the Google product taxonomy. Categorizing the product helps ensure that the ad is shown with the right search results | [optional] 
**merchantProductType** | **string** | Categorization used by Merchant as a complement to Google Taxonomy | [optional] 
**lineNumber** | **int** | Line number. The merchant may add a line number to each order item, to sort them in a particular order | [optional] 
**productUrl** | **string** | URL to the product | [optional] 
**marketPlaceSellerId** | **string** | ID of an individual seller on a marketplace | [optional] 
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 
**additionalInformation** | **string** | Extended description of the order item. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


