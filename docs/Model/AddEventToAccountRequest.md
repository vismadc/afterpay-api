# AddEventToAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventType** | **string** | Event type | 
**categoryType** | **string** | Category type | [optional] 
**time** | [**\DateTime**](\DateTime.md) | Date and time when event has appeared | 
**nfcId** | **string** | ID of NFC chip used in event | [optional] 
**posId** | **string** | ID of POS terminal used in event | [optional] 
**posName** | **string** | Name of POS terminal used in event | [optional] 
**totalTax** | **double** | Total tax for this event (sum of all taxes of linked items) | [optional] 
**totalAmount** | **double** | Total amount for this event (sum of all gross amounts of linked items) | [optional] 
**items** | [**\Visma\AfterPayApi\Model\EventItem[]**](EventItem.md) | Event items linked to this event | [optional] 
**currency** | **string** | Transaction currency | 
**accountTransactionId** | **string** | Transaction ID | [optional] 
**countryCode** | **string** | Country code | 
**conversationLanguage** | **string** | Conversation language. &#39;SE&#39; and &#39;DK&#39; are obsolete values, and will be removed in the future. Use &#39;SV&#39; instead of &#39;SE&#39;, and &#39;DA&#39; instead of &#39;DK&#39;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


