# AvailablePaymentMethodsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 
**additionalData** | [**\Visma\AfterPayApi\Model\AdditionalData**](AdditionalData.md) | Additional data | [optional] 
**yourReference** | **string** | Can be used by purchasing company if they would like to provide internal reference | [optional] 
**ourReference** | **string** | Can be used by selling company if they would like to provide specific reference for purchasing company | [optional] 
**customer** | [**\Visma\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | Customer | 
**deliveryCustomer** | [**\Visma\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | Delivery customer | [optional] 
**order** | [**\Visma\AfterPayApi\Model\Order**](Order.md) | Order number and details | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


