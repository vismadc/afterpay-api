# SearchOrderRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**criteria** | [**\Visma\AfterPayApi\Model\SearchOrderCriteria**](SearchOrderCriteria.md) | Search order criteria | [optional] 
**paging** | [**\Visma\AfterPayApi\Model\SearchOrderPaging**](SearchOrderPaging.md) | Search order paging | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


