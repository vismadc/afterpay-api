# SearchOrderCriteria

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdFrom** | [**\DateTime**](\DateTime.md) | Order from date | [optional] 
**createdTo** | [**\DateTime**](\DateTime.md) | Order to date | [optional] 
**parentTransactionReference** | **string** | Parent transaction reference | [optional] 
**orderNumber** | **string** | Order number | [optional] 
**name** | **string** | Customer lastname or company name. Everything over 50 characters will be truncated | [optional] 
**category** | **string** | Customer category | [optional] 
**email** | **string** | Client email | [optional] 
**orderStatus** | **string** | Order status | [optional] 
**captureStatus** | **string** | Capture status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


