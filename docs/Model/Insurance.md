# Insurance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **string** | Name of insurance company | [optional] 
**type** | **string** | Insurance type | [optional] 
**price** | **double** | Price of insurance | [optional] 
**currency** | **string** | Currency | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


