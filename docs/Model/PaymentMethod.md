# PaymentMethod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Available payment type | [optional] 
**title** | **string** | Available payment title | 
**tag** | **string** | Available payment tag line | 
**consumerFeeAmount** | **double** | Available payment consumer invoice fee | [optional] 
**logo** | **string** | Available payment logo path | 
**account** | [**\Visma\AfterPayApi\Model\AccountProduct**](AccountProduct.md) | Account product information | [optional] 
**directDebit** | [**\Visma\AfterPayApi\Model\DirectDebitInfo**](DirectDebitInfo.md) | Direct debit availability for this payment type | [optional] 
**campaigns** | [**\Visma\AfterPayApi\Model\CampaignInfo**](CampaignInfo.md) | Available campaigns for this payment type | [optional] 
**installment** | [**\Visma\AfterPayApi\Model\InstallmentInfo**](InstallmentInfo.md) | Installment information when payment type is Installment | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


