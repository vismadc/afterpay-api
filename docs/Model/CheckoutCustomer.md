# CheckoutCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerNumber** | **string** | Customer unique number | [optional] 
**identificationNumber** | **string** | National ID number (if the customer is a physical person). Registration number (if the customer is a company). | [optional] 
**salutation** | **string** | Salutation. | [optional] 
**firstName** | **string** | First name. Everything over 50 characters will be truncated | [optional] 
**lastName** | **string** | Last name. Everything over 50 characters will be truncated | [optional] 
**companyName** | **string** | Purchasing company name. Everything over 50 characters will be truncated | [optional] 
**email** | **string** | Email | [optional] 
**phone** | **string** | Phone | [optional] 
**mobilePhone** | **string** | Mobile phone | [optional] 
**birthDate** | [**\DateTime**](\DateTime.md) | Date of birth. Format is &#39;YYYY-MM-DD&#39;. Not required if customerCategory is &#39;Company&#39;. | [optional] 
**customerCategory** | **string** | Customer category. | 
**address** | [**\Visma\AfterPayApi\Model\Address**](Address.md) | Address | [optional] 
**riskData** | [**\Visma\AfterPayApi\Model\CustomerRisk**](CustomerRisk.md) | Risk related data. Merchants can do external risk checks and provide that information to AfterPay. | [optional] 
**conversationLanguage** | **string** | Conversation language. &#39;SE&#39; and &#39;DK&#39; are obsolete values, and will be removed in the future. Use &#39;SV&#39; instead of &#39;SE&#39;, and &#39;DA&#39; instead of &#39;DK&#39;. | [optional] 
**distributionType** | **string** | Specifies how information (invoices, notices, etc.) will be distributed to customer. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


