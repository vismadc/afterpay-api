# AccountProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profileNo** | **int** | Account Profile number | 
**installmentAmount** | **double** |  | [optional] 
**startupFee** | **double** |  | [optional] 
**monthlyFee** | **double** |  | [optional] 
**interestRate** | **double** |  | [optional] 
**readMore** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


