# CustomerResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerNumber** | **string** | Customer number | [optional] 
**customerAccountId** | **string** | Customer account id | [optional] 
**firstName** | **string** | First name | [optional] 
**lastName** | **string** | Last name | [optional] 
**addressList** | [**\Visma\AfterPayApi\Model\Address[]**](Address.md) | Address list | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


