# ValidateAddressResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isValid** | **bool** | Is response valid | [optional] 
**correctedAddress** | [**\Visma\AfterPayApi\Model\Address**](Address.md) | Corrected address | [optional] 
**riskCheckMessages** | [**\Visma\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


