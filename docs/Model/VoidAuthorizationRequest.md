# VoidAuthorizationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cancellationDetails** | [**\Visma\AfterPayApi\Model\OrderSummary**](OrderSummary.md) | Information that is used when the order needs to be canceled. Required for Partial Void | 
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 
**merchantId** | **string** | Merchant ID within the AfterPay system, part of the credentials used for AfterPay Netherlands and Belgium. If you were not provided these credentials by your Account Manager, leave empty. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


