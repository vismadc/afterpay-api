# CreateContractResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contractId** | **string** | Contract ID | [optional] 
**requireCustomerConfirmation** | **bool** | RequireCustomerConfirmation | [optional] 
**contractList** | [**\Visma\AfterPayApi\Model\Contract[]**](Contract.md) | Contract list | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


