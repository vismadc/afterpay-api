# Train

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**passengers** | [**\Visma\AfterPayApi\Model\Passenger[]**](Passenger.md) | Passengers | [optional] 
**itineraries** | [**\Visma\AfterPayApi\Model\Itinerary[]**](Itinerary.md) | Itineraries | [optional] 
**insurance** | [**\Visma\AfterPayApi\Model\Insurance**](Insurance.md) | Insurance | [optional] 
**bookingReference** | **string** | Booking reference | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


