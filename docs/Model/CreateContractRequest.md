# CreateContractRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentInfo** | [**\Visma\AfterPayApi\Model\Payment**](Payment.md) | Payment info | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


