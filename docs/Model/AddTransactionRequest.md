# AddTransactionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactionType** | **string** | Transaction type | 
**amount** | **double** | Transaction amount | 
**currency** | **string** | Transaction currency | 
**accountTransactionId** | **string** | Transaction ID | [optional] 
**additionalTransactionInfo** | [**\Visma\AfterPayApi\Model\AdditionalTransactionInfo**](AdditionalTransactionInfo.md) | Additional transaction info | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


