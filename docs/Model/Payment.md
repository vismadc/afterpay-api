# Payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Payment method | 
**contractId** | **string** | Contract ID | [optional] 
**directDebit** | [**\Visma\AfterPayApi\Model\DirectDebit**](DirectDebit.md) | Direct debit information | [optional] 
**campaign** | [**\Visma\AfterPayApi\Model\Campaign**](Campaign.md) | Campaign information | [optional] 
**invoice** | [**\Visma\AfterPayApi\Model\Invoice**](Invoice.md) | Invoice information. Reserved for future use. | [optional] 
**account** | [**\Visma\AfterPayApi\Model\SelectedAccount**](SelectedAccount.md) | Used when consumer has chosen flexible installment plan (minimum amount to pay) | [optional] 
**consolidatedInvoice** | [**\Visma\AfterPayApi\Model\ConsolidatedInvoice**](ConsolidatedInvoice.md) | Consolidate invoice information | [optional] 
**installment** | [**\Visma\AfterPayApi\Model\Installment**](Installment.md) | Installment information | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


