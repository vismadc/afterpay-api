# SearchOrderPagingWithTotals

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pageCount** | **int** | Total number of pages | [optional] 
**orderCount** | **int** | Total number of orders | [optional] 
**pageNumber** | **int** | Page number | [optional] 
**pageSize** | **int** | Number of results per page. Maximum allowed 1000 results per page. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


