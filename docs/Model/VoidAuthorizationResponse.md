# VoidAuthorizationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalCapturedAmount** | **double** | Total captured amount | [optional] 
**totalAuthorizedAmount** | **double** | Total authorized amount | [optional] 
**remainingAuthorizedAmount** | **double** | If Total Captured and Total Authorized amount differ, the remaining sum must also be captured or voided. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


