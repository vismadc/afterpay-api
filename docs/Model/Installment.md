# Installment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profileNo** | **int** | Installment profile number | 
**numberOfInstallments** | **int** | Number of installments | [optional] 
**customerInterestRate** | **double** | Customer interest rate | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


