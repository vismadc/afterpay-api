# ShippingDetailsWithNumber

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shippingNumber** | **int** | Unique shipping number within one capture | 
**type** | **string** | Shipping type | 
**shippingCompany** | **string** | Company name providing shipping | 
**trackingId** | **string** | Tracking ID | 
**trackingUrl** | **string** | Webpage URL to track shipping status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


