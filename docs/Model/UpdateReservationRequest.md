# UpdateReservationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderHandle** | **string** | Reservation identifier | [optional] 
**strongAuthenticationToken** | **string** | Strong Authentication Token (e.g. Signicat) | [optional] 
**identificationNumber** | **string** | National ID number (if the customer is a physical person). Registration number (if the customer is a company). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


