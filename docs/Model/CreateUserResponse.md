# CreateUserResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userCreated** | **bool** | Indicates whether user was created or not | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


