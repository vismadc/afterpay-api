# ResponseMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Message type | [optional] 
**code** | **string** | Confirmation or error code | [optional] 
**message** | **string** | Message content | [optional] 
**customerFacingMessage** | **string** | Message to display to customer | [optional] 
**actionCode** | **string** | Possible next action to make | [optional] 
**fieldReference** | **string** | Reference to field that caused an error | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


