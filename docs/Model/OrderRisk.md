# OrderRisk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelType** | **string** | Order channel type | [optional] 
**deliveryType** | **string** | Order delivery type | [optional] 
**ticketDeliveryMethod** | **string** | Name of the tickets&#39; method | [optional] 
**airline** | [**\Visma\AfterPayApi\Model\Airline**](Airline.md) | Airline information when airline tickets or services are ordered | [optional] 
**bus** | [**\Visma\AfterPayApi\Model\Bus**](Bus.md) | Bus information when bus tickets or services are ordered | [optional] 
**train** | [**\Visma\AfterPayApi\Model\Train**](Train.md) | Train information when train tickets or services are ordered | [optional] 
**ferry** | [**\Visma\AfterPayApi\Model\Ferry**](Ferry.md) | Ferry information when ferry tickets or services are ordered | [optional] 
**rental** | [**\Visma\AfterPayApi\Model\Rental**](Rental.md) | Rental car information | [optional] 
**hotel** | [**\Visma\AfterPayApi\Model\Hotel**](Hotel.md) | Hotel information | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


