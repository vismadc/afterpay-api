# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **string** | Name of street or packstation | [optional] 
**streetNumber** | **string** | Street or packstation number | [optional] 
**streetNumberAdditional** | **string** | Additional street number | [optional] 
**postalCode** | **string** | Postal code | [optional] 
**postalPlace** | **string** | Postal place | [optional] 
**countryCode** | **string** | Country code | [optional] 
**addressType** | **string** | Address type | [optional] 
**careOf** | **string** | Care of. Intermediary who is responsible for transferring a piece of mail between the postal system and the final addressee. For example Jane c/o John (“Jane at John&#39;s address”).  This field has to be used for company, authority and organization names as well - e. g. \&quot;Sportverein Blau-Weiß e.V. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


