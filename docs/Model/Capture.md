# Capture

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservationId** | **string** | Reservation ID | [optional] 
**customerNumber** | **string** | Customer number | [optional] 
**captureNumber** | **string** | Capture number | [optional] 
**orderNumber** | **string** | Order number | [optional] 
**amount** | **double** | Shows the original invoice amount | [optional] 
**balance** | **double** | Shows the remaining amount due on the invoice | [optional] 
**totalRefundedAmount** | **double** | Total refunded amount | [optional] 
**currency** | **string** | Currency code | [optional] 
**insertedAt** | [**\DateTime**](\DateTime.md) | Indicates the capture creation time | [optional] 
**updatedAt** | [**\DateTime**](\DateTime.md) | Indicates the time when the capture was last updated | [optional] 
**directDebitBankAccount** | **string** | Direct debit bank account | [optional] 
**directDebitSwift** | **string** | Direct debit swift | [optional] 
**accountProfileNumber** | **int** | Account profile number | [optional] 
**numberOfInstallments** | **int** | Number of installments | [optional] 
**installmentAmount** | **double** | Installment amount | [optional] 
**contractDate** | [**\DateTime**](\DateTime.md) | Contract date | [optional] 
**orderDate** | [**\DateTime**](\DateTime.md) | Order date | [optional] 
**installmentProfileNumber** | **int** | Installment profile number | [optional] 
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 
**transactionReference** | **string** | A unique reference for each transaction (separate for transactions within an order),  generated and provided to AfterPay by a third party (merchant or Payment Service Provider).   It is used to identify transactions during back-end settlement between Core systems and the PSP. | [optional] 
**dueDate** | [**\DateTime**](\DateTime.md) | Due date | [optional] 
**invoiceDate** | [**\DateTime**](\DateTime.md) | Invoice date | [optional] 
**yourReference** | **string** | Reference number. Only to be used if advised by AfterPay integration manager | [optional] 
**ourReference** | **string** | Reference number. Only to be used if advised by AfterPay integration manager | [optional] 
**invoiceProfileNumber** | **int** | Invoice profile number | [optional] 
**ocr** | **string** | OCR (Optical Character Recognition) number bound to this capture | [optional] 
**installmentCustomerInterestRate** | **double** | Installment customer interest rate | [optional] 
**imageUrl** | **string** | URL for the image of this product. It will be turned into a thumbnail and displayed in MyAfterPay,  on the invoice line next to the order item. The linked image must be a rectangle or square,  width between 100 pixels and 1280 pixels. | [optional] 
**captureItems** | [**\Visma\AfterPayApi\Model\CaptureItem[]**](CaptureItem.md) | Capture items | [optional] 
**shippingDetails** | [**\Visma\AfterPayApi\Model\ShippingDetailsWithNumber[]**](ShippingDetailsWithNumber.md) | Shipping details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


