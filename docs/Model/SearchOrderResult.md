# SearchOrderResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **string** | Customer category | [optional] 
**parentTransactionReference** | **string** | Parent transaction reference | [optional] 
**orderNumber** | **string** | Order number | [optional] 
**orderCreated** | [**\DateTime**](\DateTime.md) | Order timestamp | [optional] 
**name** | **string** | Customer name or company name | [optional] 
**totalAmount** | **double** | Order total gross amount | [optional] 
**reservedAmount** | **double** | Order reserved (non-captured) amount | [optional] 
**capturedAmount** | **double** | Order captured amount | [optional] 
**invoicedAmount** | **double** | Order captured amount | [optional] 
**orderStatus** | **string** | Order status | [optional] 
**captureStatus** | **string** | Capture status | [optional] 
**clientId** | **int** | Client id | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


