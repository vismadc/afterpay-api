# Contract

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contractType** | **string** | Contract type | [optional] 
**contractContent** | **string** | Contract content | [optional] 
**contractNumber** | **string** | Contract number | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


