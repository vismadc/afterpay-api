# CustomerLookupRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Customer email address | [optional] 
**mobilePhone** | **string** | Phone number | [optional] 
**countryCode** | **string** | Country code | [optional] 
**postalCode** | **string** | Postal code | [optional] 
**identificationNumber** | **string** | Social security number / Organization number | [optional] 
**customerNumber** | **string** | Customer number | [optional] 
**customerCategory** | **string** | Customer category | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


