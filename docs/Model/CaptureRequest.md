# CaptureRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderDetails** | [**\Visma\AfterPayApi\Model\OrderSummary**](OrderSummary.md) | Order details | 
**invoiceNumber** | **string** | Invoice number. The merchant may want to use internal invoice numbers, or let the AfterPay generate one at random.  This will show up in AfterPay&#39;s portal. One order may have one or more invoices. | [optional] 
**campaignNumber** | **int** | Invoice campaign number. The merchant may link multiple orders to a campaign they are running. | [optional] 
**shippingDetails** | [**\Visma\AfterPayApi\Model\ShippingDetails[]**](ShippingDetails.md) | Shipping information which includes shipping type, company and tracking ID | [optional] 
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 
**transactionReference** | **string** | A unique reference for each transaction (separate for transactions within an order), generated and provided to AfterPay by a third party (merchant or Payment Service Provider).   It is used to identify transactions during back-end settlement between Core systems and the PSP. | [optional] 
**references** | [**\Visma\AfterPayApi\Model\References**](References.md) | References object to provide AfterPay additional information. Only to be used if advised by AfterPay integration manager. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


