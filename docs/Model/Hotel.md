# Hotel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **string** |  | [optional] 
**address** | [**\Visma\AfterPayApi\Model\Address**](Address.md) | Hotel address | [optional] 
**checkin** | [**\DateTime**](\DateTime.md) | Check-in date and time | [optional] 
**checkout** | [**\DateTime**](\DateTime.md) | Check-out date and time | [optional] 
**guests** | [**\Visma\AfterPayApi\Model\CheckoutCustomer[]**](CheckoutCustomer.md) | Guests information | [optional] 
**numberOfRooms** | **int** | Number of rooms | [optional] 
**price** | **double** | Price of the hotel accommodation | [optional] 
**currency** | **string** | Currency | [optional] 
**bookingReference** | **string** | Booking reference | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


