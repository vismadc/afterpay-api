# RefundOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalCapturedAmount** | **double** | The total captured amount done with either a full capture, partial capture, or multiple partial captures | [optional] 
**totalAuthorizedAmount** | **double** | Total authorized amount | [optional] 
**refundNumbers** | **string[]** | RefundNumber is assigned to an invoice that has been refunded. For one Authorization you can have multiple invoices and refundNumbers | [optional] 
**totalRefundedAmount** | **double** | Total refunded amount for this order | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


