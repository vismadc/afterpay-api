# UpdateOrderRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updateOrderSummary** | [**\Visma\AfterPayApi\Model\OrderSummary**](OrderSummary.md) | Update order | 
**yourReference** | **string** | Can be used by purchasing company if they would like to provide internal reference | [optional] 
**ourReference** | **string** | Can be used by selling company if they would like to provide specific reference for purchasing company | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


