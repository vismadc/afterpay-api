# AuthorizePaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checkoutId** | **string** | Unique identifier of checkout process in UUID format. Required only in the Two-Step Authorize use-case. | [optional] 
**merchantId** | **string** | Merchant ID within the AfterPay system, part of the credentials used for AfterPay Netherlands and Belgium. If you were not provided these credentials by your Account Manager, leave empty. | [optional] 
**payment** | [**\Visma\AfterPayApi\Model\Payment**](Payment.md) | Default payment method | 
**customer** | [**\Visma\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | CheckoutCustomer object | [optional] 
**deliveryCustomer** | [**\Visma\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | To be used if the receiver of the order differs from the billing customer | [optional] 
**order** | [**\Visma\AfterPayApi\Model\Order**](Order.md) | Order object | [optional] 
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 
**additionalData** | [**\Visma\AfterPayApi\Model\AdditionalData**](AdditionalData.md) | Additional data | [optional] 
**yourReference** | **string** | Can be used by purchasing company if they would like to provide internal reference | [optional] 
**ourReference** | **string** | Can be used by selling company if they would like to provide specific reference for purchasing company | [optional] 
**einvoiceInformation** | [**\Visma\AfterPayApi\Model\EinvoiceInformation**](EinvoiceInformation.md) | Includes information to be able to send e-invoice to customer. | [optional] 
**nonce** | **string** | Unique string provided by AfterPay if the merchant is using Hosted Fields. Contact your Key Account Manager for more details. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


