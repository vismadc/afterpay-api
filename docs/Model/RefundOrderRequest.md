# RefundOrderRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**captureNumber** | **string** | Capture number to refund. This is the capture number which comes from Capture response. Required for Partial Refund | 
**items** | [**\Visma\AfterPayApi\Model\RefundOrderItem[]**](RefundOrderItem.md) | Order items to refund. Required for Partial Refund. Maximum allowed 200 items. | 
**refundType** | **string** | Type of the refund. Please use \&quot;Return\&quot; for returned items and \&quot;Refund\&quot; for goodwill refunds. | [optional] 
**parentTransactionReference** | **string** | A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order. | [optional] 
**transactionReference** | **string** | A unique reference for each transaction (separate for transactions within an order), generated and provided to AfterPay by a third party (merchant or Payment Service Provider).   It is used to identify transactions during back-end settlement between Core systems and the PSP. | [optional] 
**creditNoteNumber** | **string** | Credit note number. The merchant may want to use internal credit note numbers, or let the AfterPay generate one based on invoice to be credited. | [optional] 
**merchantId** | **string** | Merchant ID within the AfterPay system, part of the credentials used for AfterPay Netherlands and Belgium.  If you were not provided these credentials by your Account Manager, leave empty. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


