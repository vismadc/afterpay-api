# CustomerRisk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**existingCustomer** | **bool** | Is customer an existing customer for merchant | [optional] 
**verifiedCustomerIdentification** | **bool** | Is identification verified | [optional] 
**marketingOptIn** | **bool** | Shows if merchant is allowed to send marketing information to customer | [optional] 
**customerSince** | [**\DateTime**](\DateTime.md) | Since when customer has been merchant&#39;s client | [optional] 
**customerClassification** | **string** | Customer reputation (e.g. VIP client) | [optional] 
**acquisitionChannel** | **string** | Specify the channel which consumer has used for accessing merchant page | [optional] 
**hasCustomerCard** | **bool** | Shows if customer has loyalty card | [optional] 
**customerCardSince** | [**\DateTime**](\DateTime.md) | The date when the loyalty card was issued to the customer | [optional] 
**customerCardClassification** | **string** | Specifies the level of the loyalty card (e.g Gold member).  For DE, AT, CH, this must be an integer from 1 to 5, where 5 is the highest VIP level. | [optional] 
**profileTrackingId** | **string** | Unique Id of the device for profile tracking | [optional] 
**ipAddress** | **string** | Customer’s IP address | [optional] 
**numberOfTransactions** | **int** | Total number of successful purchases that have been made by the specific consumer | [optional] 
**customerIndividualScore** | **string** | The customer&#39;s individual risk score provided by the merchant.  Accepts a number from -10000 to 10000 (can be encased in quotation marks or not), or a single case-insensitive letter from &#39;a&#39; to &#39;z&#39;. | [optional] 
**userAgent** | **string** | UserAgent of this specific consumer | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


