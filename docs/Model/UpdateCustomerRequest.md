# UpdateCustomerRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distributionType** | **string** | Specifies how information (invoices, notices, etc.) will be distributed to customer. | [optional] 
**email** | **string** | Email. Required if distributionType is &#39;Email&#39;. | [optional] 
**mobilePhone** | **string** | Mobile phone. International format including country prefix. Required if distributionType is &#39;Sms&#39;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


