# GetVoidsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cancellations** | [**\Visma\AfterPayApi\Model\Cancellations[]**](Cancellations.md) | Array of the Cancellations objects | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


