# GetReservationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerName** | **string** | Customer first name | [optional] 
**merchantName** | **string** | Merchant (client) name | [optional] 
**reservationCreatedAt** | **string** | Reservation creation date and time | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


