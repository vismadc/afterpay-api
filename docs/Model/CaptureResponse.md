# CaptureResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capturedAmount** | **double** | Captured amount | [optional] 
**authorizedAmount** | **double** | Authorized amount | [optional] 
**remainingAuthorizedAmount** | **double** | If Captured and Authorized amount differ, the remaining sum must also be captured or refunded. | [optional] 
**captureNumber** | **string** | Capture number. This is the Invoice Number from Capture Request. Created by the merchant or AfterPay.  This number is used later to refund an order | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


