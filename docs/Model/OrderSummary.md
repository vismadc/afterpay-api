# OrderSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalNetAmount** | **double** | Total net amount of order. | 
**totalGrossAmount** | **double** | Total gross amount of order. Must match the grossUnitPrice * quantity of order items | 
**currency** | **string** | Order currency. If not set it will be taken from client&#39;s source system currency setting | [optional] 
**risk** | [**\Visma\AfterPayApi\Model\OrderRisk**](OrderRisk.md) | Risk data | [optional] 
**merchantImageUrl** | **string** | Image URL for the merchants brand. This image is shown at the top of the order page in MyAfterPay | [optional] 
**imageUrl** | **string** | URL for the image of this product. It will be turned into a thumbnail and displayed in MyAfterPay, on the invoice line next to the order item.  The linked image must be a rectangle or square, width between 100 pixels and 1280 pixels. | [optional] 
**googleAnalyticsUserId** | **string** | User ID identifies a user that may interact with content using different browser instances and devices | [optional] 
**googleAnalyticsClientId** | **string** | Client ID is the identifier used by Google Analytics to represent a browser instance or device | [optional] 
**items** | [**\Visma\AfterPayApi\Model\OrderItem[]**](OrderItem.md) | Array of order items. Maximum allowed 200 items. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


