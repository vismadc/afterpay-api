# AvailablePaymentMethodsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**riskCheckMessages** | [**\Visma\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 
**deliveryCustomer** | [**\Visma\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Delivery customer | [optional] 
**checkoutId** | **string** | Unique identifier of checkout process in UUID format. | [optional] 
**outcome** | **string** | Outcome | [optional] 
**customer** | [**\Visma\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Customer | [optional] 
**paymentMethods** | [**\Visma\AfterPayApi\Model\PaymentMethod[]**](PaymentMethod.md) | Allowed payment methods | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


