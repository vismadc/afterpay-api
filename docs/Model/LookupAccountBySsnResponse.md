# LookupAccountBySsnResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**\Visma\AfterPayApi\Model\Account**](Account.md) |  | [optional] 
**customer** | [**\Visma\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Customer details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


