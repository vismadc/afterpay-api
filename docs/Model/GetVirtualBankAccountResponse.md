# GetVirtualBankAccountResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**virtualBankAccount** | **string** | Individual virtual bank account for a customer | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


