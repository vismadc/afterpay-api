# SearchOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**\Visma\AfterPayApi\Model\SearchOrderResult[]**](SearchOrderResult.md) | List of orders | [optional] 
**paging** | [**\Visma\AfterPayApi\Model\SearchOrderPagingWithTotals**](SearchOrderPagingWithTotals.md) | Paging info | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


