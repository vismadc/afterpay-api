<?php
/**
 * PaymentMethod
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Visma\AfterPayApi\Model;

/**
 * PaymentMethod Class Doc Comment
 *
 * @category    Class
 * @description Payment method information
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PaymentMethod extends ArrayModel
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'PaymentMethod';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'type' => 'string',
        'title' => 'string',
        'tag' => 'string',
        'consumerFeeAmount' => 'double',
        'logo' => 'string',
        'account' => '\Visma\AfterPayApi\Model\AccountProduct',
        'directDebit' => '\Visma\AfterPayApi\Model\DirectDebitInfo',
        'campaigns' => '\Visma\AfterPayApi\Model\CampaignInfo',
        'installment' => '\Visma\AfterPayApi\Model\InstallmentInfo'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'type' => null,
        'title' => null,
        'tag' => null,
        'consumerFeeAmount' => 'double',
        'logo' => null,
        'account' => null,
        'directDebit' => null,
        'campaigns' => null,
        'installment' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'type' => 'type',
        'title' => 'title',
        'tag' => 'tag',
        'consumerFeeAmount' => 'consumerFeeAmount',
        'logo' => 'logo',
        'account' => 'account',
        'directDebit' => 'directDebit',
        'campaigns' => 'campaigns',
        'installment' => 'installment'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'type' => 'setType',
        'title' => 'setTitle',
        'tag' => 'setTag',
        'consumerFeeAmount' => 'setConsumerFeeAmount',
        'logo' => 'setLogo',
        'account' => 'setAccount',
        'directDebit' => 'setDirectDebit',
        'campaigns' => 'setCampaigns',
        'installment' => 'setInstallment'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'type' => 'getType',
        'title' => 'getTitle',
        'tag' => 'getTag',
        'consumerFeeAmount' => 'getConsumerFeeAmount',
        'logo' => 'getLogo',
        'account' => 'getAccount',
        'directDebit' => 'getDirectDebit',
        'campaigns' => 'getCampaigns',
        'installment' => 'getInstallment'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const TYPE_INVOICE = 'Invoice';
    const TYPE_ACCOUNT = 'Account';
    const TYPE_INSTALLMENT = 'Installment';
    const TYPE_CONSOLIDATEDINVOICE = 'Consolidatedinvoice';
    const TYPE_DIRECT_DEBIT_INVOICE = 'DirectDebitInvoice';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getTypeAllowableValues()
    {
        return [
            self::TYPE_INVOICE,
            self::TYPE_ACCOUNT,
            self::TYPE_INSTALLMENT,
            self::TYPE_CONSOLIDATEDINVOICE,
            self::TYPE_DIRECT_DEBIT_INVOICE,
        ];
    }
    

   

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['title'] = isset($data['title']) ? $data['title'] : null;
        $this->container['tag'] = isset($data['tag']) ? $data['tag'] : null;
        $this->container['consumerFeeAmount'] = isset($data['consumerFeeAmount']) ? $data['consumerFeeAmount'] : null;
        $this->container['logo'] = isset($data['logo']) ? $data['logo'] : null;
        $this->container['account'] = isset($data['account']) ? $data['account'] : null;
        $this->container['directDebit'] = isset($data['directDebit']) ? $data['directDebit'] : null;
        $this->container['campaigns'] = isset($data['campaigns']) ? $data['campaigns'] : null;
        $this->container['installment'] = isset($data['installment']) ? $data['installment'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        $allowed_values = $this->getTypeAllowableValues();
        if (!in_array($this->container['type'], $allowed_values)) {
            $invalid_properties[] = sprintf(
                "invalid value for 'type', must be one of '%s'",
                implode("', '", $allowed_values)
            );
        }

        if ($this->container['title'] === null) {
            $invalid_properties[] = "'title' can't be null";
        }
        if ($this->container['tag'] === null) {
            $invalid_properties[] = "'tag' can't be null";
        }
        if ($this->container['logo'] === null) {
            $invalid_properties[] = "'logo' can't be null";
        }
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        $allowed_values = $this->getTypeAllowableValues();
        if (!in_array($this->container['type'], $allowed_values)) {
            return false;
        }
        if ($this->container['title'] === null) {
            return false;
        }
        if ($this->container['tag'] === null) {
            return false;
        }
        if ($this->container['logo'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets type
     * @return string
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     * @param string $type Available payment type
     * @return $this
     */
    public function setType($type)
    {
        $allowed_values = $this->getTypeAllowableValues();
        if (!is_null($type) && !in_array($type, $allowed_values)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'type', must be one of '%s'",
                    implode("', '", $allowed_values)
                )
            );
        }
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets title
     * @return string
     */
    public function getTitle()
    {
        return $this->container['title'];
    }

    /**
     * Sets title
     * @param string $title Available payment title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->container['title'] = $title;

        return $this;
    }

    /**
     * Gets tag
     * @return string
     */
    public function getTag()
    {
        return $this->container['tag'];
    }

    /**
     * Sets tag
     * @param string $tag Available payment tag line
     * @return $this
     */
    public function setTag($tag)
    {
        $this->container['tag'] = $tag;

        return $this;
    }

    /**
     * Gets consumerFeeAmount
     * @return double
     */
    public function getConsumerFeeAmount()
    {
        return $this->container['consumerFeeAmount'];
    }

    /**
     * Sets consumerFeeAmount
     * @param double $consumerFeeAmount Available payment consumer invoice fee
     * @return $this
     */
    public function setConsumerFeeAmount($consumerFeeAmount)
    {
        $this->container['consumerFeeAmount'] = $consumerFeeAmount;

        return $this;
    }

    /**
     * Gets logo
     * @return string
     */
    public function getLogo()
    {
        return $this->container['logo'];
    }

    /**
     * Sets logo
     * @param string $logo Available payment logo path
     * @return $this
     */
    public function setLogo($logo)
    {
        $this->container['logo'] = $logo;

        return $this;
    }

    /**
     * Gets account
     * @return \Visma\AfterPayApi\Model\AccountProduct
     */
    public function getAccount()
    {
        return $this->container['account'];
    }

    /**
     * Sets account
     * @param \Visma\AfterPayApi\Model\AccountProduct $account Account product information
     * @return $this
     */
    public function setAccount($account)
    {
        $this->container['account'] = $account;

        return $this;
    }

    /**
     * Gets directDebit
     * @return \Visma\AfterPayApi\Model\DirectDebitInfo
     */
    public function getDirectDebit()
    {
        return $this->container['directDebit'];
    }

    /**
     * Sets directDebit
     * @param \Visma\AfterPayApi\Model\DirectDebitInfo $directDebit Direct debit availability for this payment type
     * @return $this
     */
    public function setDirectDebit($directDebit)
    {
        $this->container['directDebit'] = $directDebit;

        return $this;
    }

    /**
     * Gets campaigns
     * @return \Visma\AfterPayApi\Model\CampaignInfo
     */
    public function getCampaigns()
    {
        return $this->container['campaigns'];
    }

    /**
     * Sets campaigns
     * @param \Visma\AfterPayApi\Model\CampaignInfo $campaigns Available campaigns for this payment type
     * @return $this
     */
    public function setCampaigns($campaigns)
    {
        $this->container['campaigns'] = $campaigns;

        return $this;
    }

    /**
     * Gets installment
     * @return \Visma\AfterPayApi\Model\InstallmentInfo
     */
    public function getInstallment()
    {
        return $this->container['installment'];
    }

    /**
     * Sets installment
     * @param \Visma\AfterPayApi\Model\InstallmentInfo $installment Installment information when payment type is Installment
     * @return $this
     */
    public function setInstallment($installment)
    {
        $this->container['installment'] = $installment;

        return $this;
    }
    
}


