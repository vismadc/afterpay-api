<?php
/**
 * AvailablePaymentMethodsResponse
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Visma\AfterPayApi\Model;

/**
 * AvailablePaymentMethodsResponse Class Doc Comment
 *
 * @category    Class
 * @description Available payment methods response
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AvailablePaymentMethodsResponse extends ArrayModel
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'AvailablePaymentMethodsResponse';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'riskCheckMessages' => '\Visma\AfterPayApi\Model\ResponseMessage[]',
        'deliveryCustomer' => '\Visma\AfterPayApi\Model\CustomerResponse',
        'checkoutId' => 'string',
        'outcome' => 'string',
        'customer' => '\Visma\AfterPayApi\Model\CustomerResponse',
        'paymentMethods' => '\Visma\AfterPayApi\Model\PaymentMethod[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'riskCheckMessages' => null,
        'deliveryCustomer' => null,
        'checkoutId' => 'uuid',
        'outcome' => null,
        'customer' => null,
        'paymentMethods' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'riskCheckMessages' => 'riskCheckMessages',
        'deliveryCustomer' => 'deliveryCustomer',
        'checkoutId' => 'checkoutId',
        'outcome' => 'outcome',
        'customer' => 'customer',
        'paymentMethods' => 'paymentMethods'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'riskCheckMessages' => 'setRiskCheckMessages',
        'deliveryCustomer' => 'setDeliveryCustomer',
        'checkoutId' => 'setCheckoutId',
        'outcome' => 'setOutcome',
        'customer' => 'setCustomer',
        'paymentMethods' => 'setPaymentMethods'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'riskCheckMessages' => 'getRiskCheckMessages',
        'deliveryCustomer' => 'getDeliveryCustomer',
        'checkoutId' => 'getCheckoutId',
        'outcome' => 'getOutcome',
        'customer' => 'getCustomer',
        'paymentMethods' => 'getPaymentMethods'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const OUTCOME_ACCEPTED = 'Accepted';
    const OUTCOME_PENDING = 'Pending';
    const OUTCOME_REJECTED = 'Rejected';
    const OUTCOME_NOT_EVALUATED = 'NotEvaluated';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getOutcomeAllowableValues()
    {
        return [
            self::OUTCOME_ACCEPTED,
            self::OUTCOME_PENDING,
            self::OUTCOME_REJECTED,
            self::OUTCOME_NOT_EVALUATED,
        ];
    }

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['riskCheckMessages'] = isset($data['riskCheckMessages']) ? $data['riskCheckMessages'] : null;
        $this->container['deliveryCustomer'] = isset($data['deliveryCustomer']) ? $data['deliveryCustomer'] : null;
        $this->container['checkoutId'] = isset($data['checkoutId']) ? $data['checkoutId'] : null;
        $this->container['outcome'] = isset($data['outcome']) ? $data['outcome'] : null;
        $this->container['customer'] = isset($data['customer']) ? $data['customer'] : null;
        $this->container['paymentMethods'] = isset($data['paymentMethods']) ? $data['paymentMethods'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        $allowed_values = $this->getOutcomeAllowableValues();
        if (!in_array($this->container['outcome'], $allowed_values)) {
            $invalid_properties[] = sprintf(
                "invalid value for 'outcome', must be one of '%s'",
                implode("', '", $allowed_values)
            );
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        $allowed_values = $this->getOutcomeAllowableValues();
        if (!in_array($this->container['outcome'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets riskCheckMessages
     * @return \Visma\AfterPayApi\Model\ResponseMessage[]
     */
    public function getRiskCheckMessages()
    {
        return $this->container['riskCheckMessages'];
    }

    /**
     * Sets riskCheckMessages
     * @param \Visma\AfterPayApi\Model\ResponseMessage[] $riskCheckMessages Risk check messages
     * @return $this
     */
    public function setRiskCheckMessages($riskCheckMessages)
    {
        $this->container['riskCheckMessages'] = $riskCheckMessages;

        return $this;
    }

    /**
     * Gets deliveryCustomer
     * @return \Visma\AfterPayApi\Model\CustomerResponse
     */
    public function getDeliveryCustomer()
    {
        return $this->container['deliveryCustomer'];
    }

    /**
     * Sets deliveryCustomer
     * @param \Visma\AfterPayApi\Model\CustomerResponse $deliveryCustomer Delivery customer
     * @return $this
     */
    public function setDeliveryCustomer($deliveryCustomer)
    {
        $this->container['deliveryCustomer'] = $deliveryCustomer;

        return $this;
    }

    /**
     * Gets checkoutId
     * @return string
     */
    public function getCheckoutId()
    {
        return $this->container['checkoutId'];
    }

    /**
     * Sets checkoutId
     * @param string $checkoutId Unique identifier of checkout process in UUID format.
     * @return $this
     */
    public function setCheckoutId($checkoutId)
    {
        $this->container['checkoutId'] = $checkoutId;

        return $this;
    }

    /**
     * Gets outcome
     * @return string
     */
    public function getOutcome()
    {
        return $this->container['outcome'];
    }

    /**
     * Sets outcome
     * @param string $outcome Outcome
     * @return $this
     */
    public function setOutcome($outcome)
    {
        $allowed_values = $this->getOutcomeAllowableValues();
        if (!is_null($outcome) && !in_array($outcome, $allowed_values)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'outcome', must be one of '%s'",
                    implode("', '", $allowed_values)
                )
            );
        }
        $this->container['outcome'] = $outcome;

        return $this;
    }

    /**
     * Gets customer
     * @return \Visma\AfterPayApi\Model\CustomerResponse
     */
    public function getCustomer()
    {
        return $this->container['customer'];
    }

    /**
     * Sets customer
     * @param \Visma\AfterPayApi\Model\CustomerResponse $customer Customer
     * @return $this
     */
    public function setCustomer($customer)
    {
        $this->container['customer'] = $customer;

        return $this;
    }

    /**
     * Gets paymentMethods
     * @return \Visma\AfterPayApi\Model\PaymentMethod[]
     */
    public function getPaymentMethods()
    {
        return $this->container['paymentMethods'];
    }

    /**
     * Sets paymentMethods
     * @param \Visma\AfterPayApi\Model\PaymentMethod[] $paymentMethods Allowed payment methods
     * @return $this
     */
    public function setPaymentMethods($paymentMethods)
    {
        $this->container['paymentMethods'] = $paymentMethods;

        return $this;
    }
}


