<?php
/**
 * Capture
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Visma\AfterPayApi\Model;

/**
 * Capture Class Doc Comment
 *
 * @category    Class
 * @description Capture
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class Capture extends ArrayModel
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'Capture';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'reservationId' => 'string',
        'customerNumber' => 'string',
        'captureNumber' => 'string',
        'orderNumber' => 'string',
        'amount' => 'double',
        'balance' => 'double',
        'totalRefundedAmount' => 'double',
        'currency' => 'string',
        'insertedAt' => '\DateTime',
        'updatedAt' => '\DateTime',
        'directDebitBankAccount' => 'string',
        'directDebitSwift' => 'string',
        'accountProfileNumber' => 'int',
        'numberOfInstallments' => 'int',
        'installmentAmount' => 'double',
        'contractDate' => '\DateTime',
        'orderDate' => '\DateTime',
        'installmentProfileNumber' => 'int',
        'parentTransactionReference' => 'string',
        'transactionReference' => 'string',
        'dueDate' => '\DateTime',
        'invoiceDate' => '\DateTime',
        'yourReference' => 'string',
        'ourReference' => 'string',
        'invoiceProfileNumber' => 'int',
        'ocr' => 'string',
        'installmentCustomerInterestRate' => 'double',
        'imageUrl' => 'string',
        'captureItems' => '\Visma\AfterPayApi\Model\CaptureItem[]',
        'shippingDetails' => '\Visma\AfterPayApi\Model\ShippingDetailsWithNumber[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'reservationId' => 'uuid',
        'customerNumber' => null,
        'captureNumber' => null,
        'orderNumber' => null,
        'amount' => 'double',
        'balance' => 'double',
        'totalRefundedAmount' => 'double',
        'currency' => null,
        'insertedAt' => 'date-time',
        'updatedAt' => 'date-time',
        'directDebitBankAccount' => null,
        'directDebitSwift' => null,
        'accountProfileNumber' => 'int32',
        'numberOfInstallments' => 'int32',
        'installmentAmount' => 'double',
        'contractDate' => 'date-time',
        'orderDate' => 'date-time',
        'installmentProfileNumber' => 'int32',
        'parentTransactionReference' => null,
        'transactionReference' => null,
        'dueDate' => 'date-time',
        'invoiceDate' => 'date-time',
        'yourReference' => null,
        'ourReference' => null,
        'invoiceProfileNumber' => 'int32',
        'ocr' => null,
        'installmentCustomerInterestRate' => 'double',
        'imageUrl' => null,
        'captureItems' => null,
        'shippingDetails' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'reservationId' => 'reservationId',
        'customerNumber' => 'customerNumber',
        'captureNumber' => 'captureNumber',
        'orderNumber' => 'orderNumber',
        'amount' => 'amount',
        'balance' => 'balance',
        'totalRefundedAmount' => 'totalRefundedAmount',
        'currency' => 'currency',
        'insertedAt' => 'insertedAt',
        'updatedAt' => 'updatedAt',
        'directDebitBankAccount' => 'directDebitBankAccount',
        'directDebitSwift' => 'directDebitSwift',
        'accountProfileNumber' => 'accountProfileNumber',
        'numberOfInstallments' => 'numberOfInstallments',
        'installmentAmount' => 'installmentAmount',
        'contractDate' => 'contractDate',
        'orderDate' => 'orderDate',
        'installmentProfileNumber' => 'installmentProfileNumber',
        'parentTransactionReference' => 'parentTransactionReference',
        'transactionReference' => 'transactionReference',
        'dueDate' => 'dueDate',
        'invoiceDate' => 'invoiceDate',
        'yourReference' => 'yourReference',
        'ourReference' => 'ourReference',
        'invoiceProfileNumber' => 'invoiceProfileNumber',
        'ocr' => 'ocr',
        'installmentCustomerInterestRate' => 'installmentCustomerInterestRate',
        'imageUrl' => 'imageUrl',
        'captureItems' => 'captureItems',
        'shippingDetails' => 'shippingDetails'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'reservationId' => 'setReservationId',
        'customerNumber' => 'setCustomerNumber',
        'captureNumber' => 'setCaptureNumber',
        'orderNumber' => 'setOrderNumber',
        'amount' => 'setAmount',
        'balance' => 'setBalance',
        'totalRefundedAmount' => 'setTotalRefundedAmount',
        'currency' => 'setCurrency',
        'insertedAt' => 'setInsertedAt',
        'updatedAt' => 'setUpdatedAt',
        'directDebitBankAccount' => 'setDirectDebitBankAccount',
        'directDebitSwift' => 'setDirectDebitSwift',
        'accountProfileNumber' => 'setAccountProfileNumber',
        'numberOfInstallments' => 'setNumberOfInstallments',
        'installmentAmount' => 'setInstallmentAmount',
        'contractDate' => 'setContractDate',
        'orderDate' => 'setOrderDate',
        'installmentProfileNumber' => 'setInstallmentProfileNumber',
        'parentTransactionReference' => 'setParentTransactionReference',
        'transactionReference' => 'setTransactionReference',
        'dueDate' => 'setDueDate',
        'invoiceDate' => 'setInvoiceDate',
        'yourReference' => 'setYourReference',
        'ourReference' => 'setOurReference',
        'invoiceProfileNumber' => 'setInvoiceProfileNumber',
        'ocr' => 'setOcr',
        'installmentCustomerInterestRate' => 'setInstallmentCustomerInterestRate',
        'imageUrl' => 'setImageUrl',
        'captureItems' => 'setCaptureItems',
        'shippingDetails' => 'setShippingDetails'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'reservationId' => 'getReservationId',
        'customerNumber' => 'getCustomerNumber',
        'captureNumber' => 'getCaptureNumber',
        'orderNumber' => 'getOrderNumber',
        'amount' => 'getAmount',
        'balance' => 'getBalance',
        'totalRefundedAmount' => 'getTotalRefundedAmount',
        'currency' => 'getCurrency',
        'insertedAt' => 'getInsertedAt',
        'updatedAt' => 'getUpdatedAt',
        'directDebitBankAccount' => 'getDirectDebitBankAccount',
        'directDebitSwift' => 'getDirectDebitSwift',
        'accountProfileNumber' => 'getAccountProfileNumber',
        'numberOfInstallments' => 'getNumberOfInstallments',
        'installmentAmount' => 'getInstallmentAmount',
        'contractDate' => 'getContractDate',
        'orderDate' => 'getOrderDate',
        'installmentProfileNumber' => 'getInstallmentProfileNumber',
        'parentTransactionReference' => 'getParentTransactionReference',
        'transactionReference' => 'getTransactionReference',
        'dueDate' => 'getDueDate',
        'invoiceDate' => 'getInvoiceDate',
        'yourReference' => 'getYourReference',
        'ourReference' => 'getOurReference',
        'invoiceProfileNumber' => 'getInvoiceProfileNumber',
        'ocr' => 'getOcr',
        'installmentCustomerInterestRate' => 'getInstallmentCustomerInterestRate',
        'imageUrl' => 'getImageUrl',
        'captureItems' => 'getCaptureItems',
        'shippingDetails' => 'getShippingDetails'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const CURRENCY_EUR = 'EUR';
    const CURRENCY_NOK = 'NOK';
    const CURRENCY_SEK = 'SEK';
    const CURRENCY_DKK = 'DKK';
    const CURRENCY_CHF = 'CHF';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getCurrencyAllowableValues()
    {
        return [
            self::CURRENCY_EUR,
            self::CURRENCY_NOK,
            self::CURRENCY_SEK,
            self::CURRENCY_DKK,
            self::CURRENCY_CHF,
        ];
    }

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['reservationId'] = isset($data['reservationId']) ? $data['reservationId'] : null;
        $this->container['customerNumber'] = isset($data['customerNumber']) ? $data['customerNumber'] : null;
        $this->container['captureNumber'] = isset($data['captureNumber']) ? $data['captureNumber'] : null;
        $this->container['orderNumber'] = isset($data['orderNumber']) ? $data['orderNumber'] : null;
        $this->container['amount'] = isset($data['amount']) ? $data['amount'] : null;
        $this->container['balance'] = isset($data['balance']) ? $data['balance'] : null;
        $this->container['totalRefundedAmount'] = isset($data['totalRefundedAmount']) ? $data['totalRefundedAmount'] : null;
        $this->container['currency'] = isset($data['currency']) ? $data['currency'] : null;
        $this->container['insertedAt'] = isset($data['insertedAt']) ? $data['insertedAt'] : null;
        $this->container['updatedAt'] = isset($data['updatedAt']) ? $data['updatedAt'] : null;
        $this->container['directDebitBankAccount'] = isset($data['directDebitBankAccount']) ? $data['directDebitBankAccount'] : null;
        $this->container['directDebitSwift'] = isset($data['directDebitSwift']) ? $data['directDebitSwift'] : null;
        $this->container['accountProfileNumber'] = isset($data['accountProfileNumber']) ? $data['accountProfileNumber'] : null;
        $this->container['numberOfInstallments'] = isset($data['numberOfInstallments']) ? $data['numberOfInstallments'] : null;
        $this->container['installmentAmount'] = isset($data['installmentAmount']) ? $data['installmentAmount'] : null;
        $this->container['contractDate'] = isset($data['contractDate']) ? $data['contractDate'] : null;
        $this->container['orderDate'] = isset($data['orderDate']) ? $data['orderDate'] : null;
        $this->container['installmentProfileNumber'] = isset($data['installmentProfileNumber']) ? $data['installmentProfileNumber'] : null;
        $this->container['parentTransactionReference'] = isset($data['parentTransactionReference']) ? $data['parentTransactionReference'] : null;
        $this->container['transactionReference'] = isset($data['transactionReference']) ? $data['transactionReference'] : null;
        $this->container['dueDate'] = isset($data['dueDate']) ? $data['dueDate'] : null;
        $this->container['invoiceDate'] = isset($data['invoiceDate']) ? $data['invoiceDate'] : null;
        $this->container['yourReference'] = isset($data['yourReference']) ? $data['yourReference'] : null;
        $this->container['ourReference'] = isset($data['ourReference']) ? $data['ourReference'] : null;
        $this->container['invoiceProfileNumber'] = isset($data['invoiceProfileNumber']) ? $data['invoiceProfileNumber'] : null;
        $this->container['ocr'] = isset($data['ocr']) ? $data['ocr'] : null;
        $this->container['installmentCustomerInterestRate'] = isset($data['installmentCustomerInterestRate']) ? $data['installmentCustomerInterestRate'] : null;
        $this->container['imageUrl'] = isset($data['imageUrl']) ? $data['imageUrl'] : null;
        $this->container['captureItems'] = isset($data['captureItems']) ? $data['captureItems'] : null;
        $this->container['shippingDetails'] = isset($data['shippingDetails']) ? $data['shippingDetails'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if (!is_null($this->container['customerNumber']) && (strlen($this->container['customerNumber']) > 100)) {
            $invalid_properties[] = "invalid value for 'customerNumber', the character length must be smaller than or equal to 100.";
        }

        if (!is_null($this->container['captureNumber']) && (strlen($this->container['captureNumber']) > 50)) {
            $invalid_properties[] = "invalid value for 'captureNumber', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['orderNumber']) && (strlen($this->container['orderNumber']) > 50)) {
            $invalid_properties[] = "invalid value for 'orderNumber', the character length must be smaller than or equal to 50.";
        }

        $allowed_values = $this->getCurrencyAllowableValues();
        if (!in_array($this->container['currency'], $allowed_values)) {
            $invalid_properties[] = sprintf(
                "invalid value for 'currency', must be one of '%s'",
                implode("', '", $allowed_values)
            );
        }

        if (!is_null($this->container['directDebitBankAccount']) && (strlen($this->container['directDebitBankAccount']) > 34)) {
            $invalid_properties[] = "invalid value for 'directDebitBankAccount', the character length must be smaller than or equal to 34.";
        }

        if (!is_null($this->container['directDebitSwift']) && (strlen($this->container['directDebitSwift']) > 11)) {
            $invalid_properties[] = "invalid value for 'directDebitSwift', the character length must be smaller than or equal to 11.";
        }

        if (!is_null($this->container['parentTransactionReference']) && (strlen($this->container['parentTransactionReference']) > 128)) {
            $invalid_properties[] = "invalid value for 'parentTransactionReference', the character length must be smaller than or equal to 128.";
        }

        if (!is_null($this->container['transactionReference']) && (strlen($this->container['transactionReference']) > 100)) {
            $invalid_properties[] = "invalid value for 'transactionReference', the character length must be smaller than or equal to 100.";
        }

        if (!is_null($this->container['yourReference']) && (strlen($this->container['yourReference']) > 20)) {
            $invalid_properties[] = "invalid value for 'yourReference', the character length must be smaller than or equal to 20.";
        }

        if (!is_null($this->container['ourReference']) && (strlen($this->container['ourReference']) > 20)) {
            $invalid_properties[] = "invalid value for 'ourReference', the character length must be smaller than or equal to 20.";
        }

        if (!is_null($this->container['ocr']) && (strlen($this->container['ocr']) > 27)) {
            $invalid_properties[] = "invalid value for 'ocr', the character length must be smaller than or equal to 27.";
        }

        if (!is_null($this->container['imageUrl']) && (strlen($this->container['imageUrl']) > 2048)) {
            $invalid_properties[] = "invalid value for 'imageUrl', the character length must be smaller than or equal to 2048.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if (strlen($this->container['customerNumber']) > 100) {
            return false;
        }
        if (strlen($this->container['captureNumber']) > 50) {
            return false;
        }
        if (strlen($this->container['orderNumber']) > 50) {
            return false;
        }
        $allowed_values = $this->getCurrencyAllowableValues();
        if (!in_array($this->container['currency'], $allowed_values)) {
            return false;
        }
        if (strlen($this->container['directDebitBankAccount']) > 34) {
            return false;
        }
        if (strlen($this->container['directDebitSwift']) > 11) {
            return false;
        }
        if (strlen($this->container['parentTransactionReference']) > 128) {
            return false;
        }
        if (strlen($this->container['transactionReference']) > 100) {
            return false;
        }
        if (strlen($this->container['yourReference']) > 20) {
            return false;
        }
        if (strlen($this->container['ourReference']) > 20) {
            return false;
        }
        if (strlen($this->container['ocr']) > 27) {
            return false;
        }
        if (strlen($this->container['imageUrl']) > 2048) {
            return false;
        }
        return true;
    }


    /**
     * Gets reservationId
     * @return string
     */
    public function getReservationId()
    {
        return $this->container['reservationId'];
    }

    /**
     * Sets reservationId
     * @param string $reservationId Reservation ID
     * @return $this
     */
    public function setReservationId($reservationId)
    {
        $this->container['reservationId'] = $reservationId;

        return $this;
    }

    /**
     * Gets customerNumber
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->container['customerNumber'];
    }

    /**
     * Sets customerNumber
     * @param string $customerNumber Customer number
     * @return $this
     */
    public function setCustomerNumber($customerNumber)
    {
        if (!is_null($customerNumber) && (strlen($customerNumber) > 100)) {
            throw new \InvalidArgumentException('invalid length for $customerNumber when calling Capture., must be smaller than or equal to 100.');
        }

        $this->container['customerNumber'] = $customerNumber;

        return $this;
    }

    /**
     * Gets captureNumber
     * @return string
     */
    public function getCaptureNumber()
    {
        return $this->container['captureNumber'];
    }

    /**
     * Sets captureNumber
     * @param string $captureNumber Capture number
     * @return $this
     */
    public function setCaptureNumber($captureNumber)
    {
        if (!is_null($captureNumber) && (strlen($captureNumber) > 50)) {
            throw new \InvalidArgumentException('invalid length for $captureNumber when calling Capture., must be smaller than or equal to 50.');
        }

        $this->container['captureNumber'] = $captureNumber;

        return $this;
    }

    /**
     * Gets orderNumber
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->container['orderNumber'];
    }

    /**
     * Sets orderNumber
     * @param string $orderNumber Order number
     * @return $this
     */
    public function setOrderNumber($orderNumber)
    {
        if (!is_null($orderNumber) && (strlen($orderNumber) > 50)) {
            throw new \InvalidArgumentException('invalid length for $orderNumber when calling Capture., must be smaller than or equal to 50.');
        }

        $this->container['orderNumber'] = $orderNumber;

        return $this;
    }

    /**
     * Gets amount
     * @return double
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     * @param double $amount Shows the original invoice amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets balance
     * @return double
     */
    public function getBalance()
    {
        return $this->container['balance'];
    }

    /**
     * Sets balance
     * @param double $balance Shows the remaining amount due on the invoice
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->container['balance'] = $balance;

        return $this;
    }

    /**
     * Gets totalRefundedAmount
     * @return double
     */
    public function getTotalRefundedAmount()
    {
        return $this->container['totalRefundedAmount'];
    }

    /**
     * Sets totalRefundedAmount
     * @param double $totalRefundedAmount Total refunded amount
     * @return $this
     */
    public function setTotalRefundedAmount($totalRefundedAmount)
    {
        $this->container['totalRefundedAmount'] = $totalRefundedAmount;

        return $this;
    }

    /**
     * Gets currency
     * @return string
     */
    public function getCurrency()
    {
        return $this->container['currency'];
    }

    /**
     * Sets currency
     * @param string $currency Currency code
     * @return $this
     */
    public function setCurrency($currency)
    {
        $allowed_values = $this->getCurrencyAllowableValues();
        if (!is_null($currency) && !in_array($currency, $allowed_values)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'currency', must be one of '%s'",
                    implode("', '", $allowed_values)
                )
            );
        }
        $this->container['currency'] = $currency;

        return $this;
    }

    /**
     * Gets insertedAt
     * @return \DateTime
     */
    public function getInsertedAt()
    {
        return $this->container['insertedAt'];
    }

    /**
     * Sets insertedAt
     * @param \DateTime $insertedAt Indicates the capture creation time
     * @return $this
     */
    public function setInsertedAt($insertedAt)
    {
        $this->container['insertedAt'] = $insertedAt;

        return $this;
    }

    /**
     * Gets updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->container['updatedAt'];
    }

    /**
     * Sets updatedAt
     * @param \DateTime $updatedAt Indicates the time when the capture was last updated
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->container['updatedAt'] = $updatedAt;

        return $this;
    }

    /**
     * Gets directDebitBankAccount
     * @return string
     */
    public function getDirectDebitBankAccount()
    {
        return $this->container['directDebitBankAccount'];
    }

    /**
     * Sets directDebitBankAccount
     * @param string $directDebitBankAccount Direct debit bank account
     * @return $this
     */
    public function setDirectDebitBankAccount($directDebitBankAccount)
    {
        if (!is_null($directDebitBankAccount) && (strlen($directDebitBankAccount) > 34)) {
            throw new \InvalidArgumentException('invalid length for $directDebitBankAccount when calling Capture., must be smaller than or equal to 34.');
        }

        $this->container['directDebitBankAccount'] = $directDebitBankAccount;

        return $this;
    }

    /**
     * Gets directDebitSwift
     * @return string
     */
    public function getDirectDebitSwift()
    {
        return $this->container['directDebitSwift'];
    }

    /**
     * Sets directDebitSwift
     * @param string $directDebitSwift Direct debit swift
     * @return $this
     */
    public function setDirectDebitSwift($directDebitSwift)
    {
        if (!is_null($directDebitSwift) && (strlen($directDebitSwift) > 11)) {
            throw new \InvalidArgumentException('invalid length for $directDebitSwift when calling Capture., must be smaller than or equal to 11.');
        }

        $this->container['directDebitSwift'] = $directDebitSwift;

        return $this;
    }

    /**
     * Gets accountProfileNumber
     * @return int
     */
    public function getAccountProfileNumber()
    {
        return $this->container['accountProfileNumber'];
    }

    /**
     * Sets accountProfileNumber
     * @param int $accountProfileNumber Account profile number
     * @return $this
     */
    public function setAccountProfileNumber($accountProfileNumber)
    {
        $this->container['accountProfileNumber'] = $accountProfileNumber;

        return $this;
    }

    /**
     * Gets numberOfInstallments
     * @return int
     */
    public function getNumberOfInstallments()
    {
        return $this->container['numberOfInstallments'];
    }

    /**
     * Sets numberOfInstallments
     * @param int $numberOfInstallments Number of installments
     * @return $this
     */
    public function setNumberOfInstallments($numberOfInstallments)
    {
        $this->container['numberOfInstallments'] = $numberOfInstallments;

        return $this;
    }

    /**
     * Gets installmentAmount
     * @return double
     */
    public function getInstallmentAmount()
    {
        return $this->container['installmentAmount'];
    }

    /**
     * Sets installmentAmount
     * @param double $installmentAmount Installment amount
     * @return $this
     */
    public function setInstallmentAmount($installmentAmount)
    {
        $this->container['installmentAmount'] = $installmentAmount;

        return $this;
    }

    /**
     * Gets contractDate
     * @return \DateTime
     */
    public function getContractDate()
    {
        return $this->container['contractDate'];
    }

    /**
     * Sets contractDate
     * @param \DateTime $contractDate Contract date
     * @return $this
     */
    public function setContractDate($contractDate)
    {
        $this->container['contractDate'] = $contractDate;

        return $this;
    }

    /**
     * Gets orderDate
     * @return \DateTime
     */
    public function getOrderDate()
    {
        return $this->container['orderDate'];
    }

    /**
     * Sets orderDate
     * @param \DateTime $orderDate Order date
     * @return $this
     */
    public function setOrderDate($orderDate)
    {
        $this->container['orderDate'] = $orderDate;

        return $this;
    }

    /**
     * Gets installmentProfileNumber
     * @return int
     */
    public function getInstallmentProfileNumber()
    {
        return $this->container['installmentProfileNumber'];
    }

    /**
     * Sets installmentProfileNumber
     * @param int $installmentProfileNumber Installment profile number
     * @return $this
     */
    public function setInstallmentProfileNumber($installmentProfileNumber)
    {
        $this->container['installmentProfileNumber'] = $installmentProfileNumber;

        return $this;
    }

    /**
     * Gets parentTransactionReference
     * @return string
     */
    public function getParentTransactionReference()
    {
        return $this->container['parentTransactionReference'];
    }

    /**
     * Sets parentTransactionReference
     * @param string $parentTransactionReference A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order.
     * @return $this
     */
    public function setParentTransactionReference($parentTransactionReference)
    {
        if (!is_null($parentTransactionReference) && (strlen($parentTransactionReference) > 128)) {
            throw new \InvalidArgumentException('invalid length for $parentTransactionReference when calling Capture., must be smaller than or equal to 128.');
        }

        $this->container['parentTransactionReference'] = $parentTransactionReference;

        return $this;
    }

    /**
     * Gets transactionReference
     * @return string
     */
    public function getTransactionReference()
    {
        return $this->container['transactionReference'];
    }

    /**
     * Sets transactionReference
     * @param string $transactionReference A unique reference for each transaction (separate for transactions within an order),  generated and provided to AfterPay by a third party (merchant or Payment Service Provider).   It is used to identify transactions during back-end settlement between Core systems and the PSP.
     * @return $this
     */
    public function setTransactionReference($transactionReference)
    {
        if (!is_null($transactionReference) && (strlen($transactionReference) > 100)) {
            throw new \InvalidArgumentException('invalid length for $transactionReference when calling Capture., must be smaller than or equal to 100.');
        }

        $this->container['transactionReference'] = $transactionReference;

        return $this;
    }

    /**
     * Gets dueDate
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->container['dueDate'];
    }

    /**
     * Sets dueDate
     * @param \DateTime $dueDate Due date
     * @return $this
     */
    public function setDueDate($dueDate)
    {
        $this->container['dueDate'] = $dueDate;

        return $this;
    }

    /**
     * Gets invoiceDate
     * @return \DateTime
     */
    public function getInvoiceDate()
    {
        return $this->container['invoiceDate'];
    }

    /**
     * Sets invoiceDate
     * @param \DateTime $invoiceDate Invoice date
     * @return $this
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->container['invoiceDate'] = $invoiceDate;

        return $this;
    }

    /**
     * Gets yourReference
     * @return string
     */
    public function getYourReference()
    {
        return $this->container['yourReference'];
    }

    /**
     * Sets yourReference
     * @param string $yourReference Reference number. Only to be used if advised by AfterPay integration manager
     * @return $this
     */
    public function setYourReference($yourReference)
    {
        if (!is_null($yourReference) && (strlen($yourReference) > 20)) {
            throw new \InvalidArgumentException('invalid length for $yourReference when calling Capture., must be smaller than or equal to 20.');
        }

        $this->container['yourReference'] = $yourReference;

        return $this;
    }

    /**
     * Gets ourReference
     * @return string
     */
    public function getOurReference()
    {
        return $this->container['ourReference'];
    }

    /**
     * Sets ourReference
     * @param string $ourReference Reference number. Only to be used if advised by AfterPay integration manager
     * @return $this
     */
    public function setOurReference($ourReference)
    {
        if (!is_null($ourReference) && (strlen($ourReference) > 20)) {
            throw new \InvalidArgumentException('invalid length for $ourReference when calling Capture., must be smaller than or equal to 20.');
        }

        $this->container['ourReference'] = $ourReference;

        return $this;
    }

    /**
     * Gets invoiceProfileNumber
     * @return int
     */
    public function getInvoiceProfileNumber()
    {
        return $this->container['invoiceProfileNumber'];
    }

    /**
     * Sets invoiceProfileNumber
     * @param int $invoiceProfileNumber Invoice profile number
     * @return $this
     */
    public function setInvoiceProfileNumber($invoiceProfileNumber)
    {
        $this->container['invoiceProfileNumber'] = $invoiceProfileNumber;

        return $this;
    }

    /**
     * Gets ocr
     * @return string
     */
    public function getOcr()
    {
        return $this->container['ocr'];
    }

    /**
     * Sets ocr
     * @param string $ocr OCR (Optical Character Recognition) number bound to this capture
     * @return $this
     */
    public function setOcr($ocr)
    {
        if (!is_null($ocr) && (strlen($ocr) > 27)) {
            throw new \InvalidArgumentException('invalid length for $ocr when calling Capture., must be smaller than or equal to 27.');
        }

        $this->container['ocr'] = $ocr;

        return $this;
    }

    /**
     * Gets installmentCustomerInterestRate
     * @return double
     */
    public function getInstallmentCustomerInterestRate()
    {
        return $this->container['installmentCustomerInterestRate'];
    }

    /**
     * Sets installmentCustomerInterestRate
     * @param double $installmentCustomerInterestRate Installment customer interest rate
     * @return $this
     */
    public function setInstallmentCustomerInterestRate($installmentCustomerInterestRate)
    {
        $this->container['installmentCustomerInterestRate'] = $installmentCustomerInterestRate;

        return $this;
    }

    /**
     * Gets imageUrl
     * @return string
     */
    public function getImageUrl()
    {
        return $this->container['imageUrl'];
    }

    /**
     * Sets imageUrl
     * @param string $imageUrl URL for the image of this product. It will be turned into a thumbnail and displayed in MyAfterPay,  on the invoice line next to the order item. The linked image must be a rectangle or square,  width between 100 pixels and 1280 pixels.
     * @return $this
     */
    public function setImageUrl($imageUrl)
    {
        if (!is_null($imageUrl) && (strlen($imageUrl) > 2048)) {
            throw new \InvalidArgumentException('invalid length for $imageUrl when calling Capture., must be smaller than or equal to 2048.');
        }

        $this->container['imageUrl'] = $imageUrl;

        return $this;
    }

    /**
     * Gets captureItems
     * @return \Visma\AfterPayApi\Model\CaptureItem[]
     */
    public function getCaptureItems()
    {
        return $this->container['captureItems'];
    }

    /**
     * Sets captureItems
     * @param \Visma\AfterPayApi\Model\CaptureItem[] $captureItems Capture items
     * @return $this
     */
    public function setCaptureItems($captureItems)
    {
        $this->container['captureItems'] = $captureItems;

        return $this;
    }

    /**
     * Gets shippingDetails
     * @return \Visma\AfterPayApi\Model\ShippingDetailsWithNumber[]
     */
    public function getShippingDetails()
    {
        return $this->container['shippingDetails'];
    }

    /**
     * Sets shippingDetails
     * @param \Visma\AfterPayApi\Model\ShippingDetailsWithNumber[] $shippingDetails Shipping details
     * @return $this
     */
    public function setShippingDetails($shippingDetails)
    {
        $this->container['shippingDetails'] = $shippingDetails;

        return $this;
    }
}


