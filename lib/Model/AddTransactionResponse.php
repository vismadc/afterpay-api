<?php
/**
 * AddTransactionResponse
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Visma\AfterPayApi\Model;

/**
 * AddTransactionResponse Class Doc Comment
 *
 * @category    Class
 * @description Add transaction to customer account response
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AddTransactionResponse extends ArrayModel
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'AddTransactionResponse';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'transactionAdded' => 'bool',
        'transationId' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'transactionAdded' => null,
        'transationId' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'transactionAdded' => 'transactionAdded',
        'transationId' => 'transationId'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'transactionAdded' => 'setTransactionAdded',
        'transationId' => 'setTransationId'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'transactionAdded' => 'getTransactionAdded',
        'transationId' => 'getTransationId'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['transactionAdded'] = isset($data['transactionAdded']) ? $data['transactionAdded'] : null;
        $this->container['transationId'] = isset($data['transationId']) ? $data['transationId'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets transactionAdded
     * @return bool
     */
    public function getTransactionAdded()
    {
        return $this->container['transactionAdded'];
    }

    /**
     * Sets transactionAdded
     * @param bool $transactionAdded Indicates whether transaction was added or not
     * @return $this
     */
    public function setTransactionAdded($transactionAdded)
    {
        $this->container['transactionAdded'] = $transactionAdded;

        return $this;
    }

    /**
     * Gets transationId
     * @return string
     */
    public function getTransationId()
    {
        return $this->container['transationId'];
    }

    /**
     * Sets transationId
     * @param string $transationId
     * @return $this
     */
    public function setTransationId($transationId)
    {
        $this->container['transationId'] = $transationId;

        return $this;
    }
}


