<?php
/**
 * RefundOrderRequest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Visma\AfterPayApi\Model;

/**
 * RefundOrderRequest Class Doc Comment
 *
 * @category    Class
 * @description Required when partially refunding a capture.
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class RefundOrderRequest extends ArrayModel
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'RefundOrderRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'captureNumber' => 'string',
        'items' => '\Visma\AfterPayApi\Model\RefundOrderItem[]',
        'refundType' => 'string',
        'parentTransactionReference' => 'string',
        'transactionReference' => 'string',
        'creditNoteNumber' => 'string',
        'merchantId' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'captureNumber' => null,
        'items' => null,
        'refundType' => null,
        'parentTransactionReference' => null,
        'transactionReference' => null,
        'creditNoteNumber' => null,
        'merchantId' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'captureNumber' => 'captureNumber',
        'items' => 'items',
        'refundType' => 'refundType',
        'parentTransactionReference' => 'parentTransactionReference',
        'transactionReference' => 'transactionReference',
        'creditNoteNumber' => 'creditNoteNumber',
        'merchantId' => 'merchantId'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'captureNumber' => 'setCaptureNumber',
        'items' => 'setItems',
        'refundType' => 'setRefundType',
        'parentTransactionReference' => 'setParentTransactionReference',
        'transactionReference' => 'setTransactionReference',
        'creditNoteNumber' => 'setCreditNoteNumber',
        'merchantId' => 'setMerchantId'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'captureNumber' => 'getCaptureNumber',
        'items' => 'getItems',
        'refundType' => 'getRefundType',
        'parentTransactionReference' => 'getParentTransactionReference',
        'transactionReference' => 'getTransactionReference',
        'creditNoteNumber' => 'getCreditNoteNumber',
        'merchantId' => 'getMerchantId'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const REFUND_TYPE_REFUND = 'Refund';
    const REFUND_TYPE__RETURN = 'Return';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getRefundTypeAllowableValues()
    {
        return [
            self::REFUND_TYPE_REFUND,
            self::REFUND_TYPE__RETURN,
        ];
    }
    

   

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['captureNumber'] = isset($data['captureNumber']) ? $data['captureNumber'] : null;
        $this->container['items'] = isset($data['items']) ? $data['items'] : null;
        $this->container['refundType'] = isset($data['refundType']) ? $data['refundType'] : null;
        $this->container['parentTransactionReference'] = isset($data['parentTransactionReference']) ? $data['parentTransactionReference'] : null;
        $this->container['transactionReference'] = isset($data['transactionReference']) ? $data['transactionReference'] : null;
        $this->container['creditNoteNumber'] = isset($data['creditNoteNumber']) ? $data['creditNoteNumber'] : null;
        $this->container['merchantId'] = isset($data['merchantId']) ? $data['merchantId'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['captureNumber'] === null) {
            $invalid_properties[] = "'captureNumber' can't be null";
        }
        if ((strlen($this->container['captureNumber']) > 50)) {
            $invalid_properties[] = "invalid value for 'captureNumber', the character length must be smaller than or equal to 50.";
        }

        if ($this->container['items'] === null) {
            $invalid_properties[] = "'items' can't be null";
        }
        $allowed_values = $this->getRefundTypeAllowableValues();
        if (!in_array($this->container['refundType'], $allowed_values)) {
            $invalid_properties[] = sprintf(
                "invalid value for 'refundType', must be one of '%s'",
                implode("', '", $allowed_values)
            );
        }

        if (!is_null($this->container['parentTransactionReference']) && (strlen($this->container['parentTransactionReference']) > 128)) {
            $invalid_properties[] = "invalid value for 'parentTransactionReference', the character length must be smaller than or equal to 128.";
        }

        if (!is_null($this->container['transactionReference']) && (strlen($this->container['transactionReference']) > 100)) {
            $invalid_properties[] = "invalid value for 'transactionReference', the character length must be smaller than or equal to 100.";
        }

        if (!is_null($this->container['creditNoteNumber']) && (strlen($this->container['creditNoteNumber']) > 19)) {
            $invalid_properties[] = "invalid value for 'creditNoteNumber', the character length must be smaller than or equal to 19.";
        }

        if (!is_null($this->container['merchantId']) && (strlen($this->container['merchantId']) > 50)) {
            $invalid_properties[] = "invalid value for 'merchantId', the character length must be smaller than or equal to 50.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['captureNumber'] === null) {
            return false;
        }
        if (strlen($this->container['captureNumber']) > 50) {
            return false;
        }
        if ($this->container['items'] === null) {
            return false;
        }
        $allowed_values = $this->getRefundTypeAllowableValues();
        if (!in_array($this->container['refundType'], $allowed_values)) {
            return false;
        }
        if (strlen($this->container['parentTransactionReference']) > 128) {
            return false;
        }
        if (strlen($this->container['transactionReference']) > 100) {
            return false;
        }
        if (strlen($this->container['creditNoteNumber']) > 19) {
            return false;
        }
        if (strlen($this->container['merchantId']) > 50) {
            return false;
        }
        return true;
    }


    /**
     * Gets captureNumber
     * @return string
     */
    public function getCaptureNumber()
    {
        return $this->container['captureNumber'];
    }

    /**
     * Sets captureNumber
     * @param string $captureNumber Capture number to refund. This is the capture number which comes from Capture response. Required for Partial Refund
     * @return $this
     */
    public function setCaptureNumber($captureNumber)
    {
        if ((strlen($captureNumber) > 50)) {
            throw new \InvalidArgumentException('invalid length for $captureNumber when calling RefundOrderRequest., must be smaller than or equal to 50.');
        }

        $this->container['captureNumber'] = $captureNumber;

        return $this;
    }

    /**
     * Gets items
     * @return \Visma\AfterPayApi\Model\RefundOrderItem[]
     */
    public function getItems()
    {
        return $this->container['items'];
    }

    /**
     * Sets items
     * @param \Visma\AfterPayApi\Model\RefundOrderItem[] $items Order items to refund. Required for Partial Refund. Maximum allowed 200 items.
     * @return $this
     */
    public function setItems($items)
    {
        $this->container['items'] = $items;

        return $this;
    }

    /**
     * Gets refundType
     * @return string
     */
    public function getRefundType()
    {
        return $this->container['refundType'];
    }

    /**
     * Sets refundType
     * @param string $refundType Type of the refund. Please use \"Return\" for returned items and \"Refund\" for goodwill refunds.
     * @return $this
     */
    public function setRefundType($refundType)
    {
        $allowed_values = $this->getRefundTypeAllowableValues();
        if (!is_null($refundType) && !in_array($refundType, $allowed_values)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'refundType', must be one of '%s'",
                    implode("', '", $allowed_values)
                )
            );
        }
        $this->container['refundType'] = $refundType;

        return $this;
    }

    /**
     * Gets parentTransactionReference
     * @return string
     */
    public function getParentTransactionReference()
    {
        return $this->container['parentTransactionReference'];
    }

    /**
     * Sets parentTransactionReference
     * @param string $parentTransactionReference A unique reference provided to AfterPay by a third party (merchant or Payment Service Provider). Identifies an entire order.
     * @return $this
     */
    public function setParentTransactionReference($parentTransactionReference)
    {
        if (!is_null($parentTransactionReference) && (strlen($parentTransactionReference) > 128)) {
            throw new \InvalidArgumentException('invalid length for $parentTransactionReference when calling RefundOrderRequest., must be smaller than or equal to 128.');
        }

        $this->container['parentTransactionReference'] = $parentTransactionReference;

        return $this;
    }

    /**
     * Gets transactionReference
     * @return string
     */
    public function getTransactionReference()
    {
        return $this->container['transactionReference'];
    }

    /**
     * Sets transactionReference
     * @param string $transactionReference A unique reference for each transaction (separate for transactions within an order), generated and provided to AfterPay by a third party (merchant or Payment Service Provider).   It is used to identify transactions during back-end settlement between Core systems and the PSP.
     * @return $this
     */
    public function setTransactionReference($transactionReference)
    {
        if (!is_null($transactionReference) && (strlen($transactionReference) > 100)) {
            throw new \InvalidArgumentException('invalid length for $transactionReference when calling RefundOrderRequest., must be smaller than or equal to 100.');
        }

        $this->container['transactionReference'] = $transactionReference;

        return $this;
    }

    /**
     * Gets creditNoteNumber
     * @return string
     */
    public function getCreditNoteNumber()
    {
        return $this->container['creditNoteNumber'];
    }

    /**
     * Sets creditNoteNumber
     * @param string $creditNoteNumber Credit note number. The merchant may want to use internal credit note numbers, or let the AfterPay generate one based on invoice to be credited.
     * @return $this
     */
    public function setCreditNoteNumber($creditNoteNumber)
    {
        if (!is_null($creditNoteNumber) && (strlen($creditNoteNumber) > 19)) {
            throw new \InvalidArgumentException('invalid length for $creditNoteNumber when calling RefundOrderRequest., must be smaller than or equal to 19.');
        }

        $this->container['creditNoteNumber'] = $creditNoteNumber;

        return $this;
    }

    /**
     * Gets merchantId
     * @return string
     */
    public function getMerchantId()
    {
        return $this->container['merchantId'];
    }

    /**
     * Sets merchantId
     * @param string $merchantId Merchant ID within the AfterPay system, part of the credentials used for AfterPay Netherlands and Belgium.  If you were not provided these credentials by your Account Manager, leave empty.
     * @return $this
     */
    public function setMerchantId($merchantId)
    {
        if (!is_null($merchantId) && (strlen($merchantId) > 50)) {
            throw new \InvalidArgumentException('invalid length for $merchantId when calling RefundOrderRequest., must be smaller than or equal to 50.');
        }

        $this->container['merchantId'] = $merchantId;

        return $this;
    }
    
}


