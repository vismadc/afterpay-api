<?php
/**
 * InstallmentInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Visma\AfterPayApi;

/**
 * InstallmentInfoTest Class Doc Comment
 *
 * @category    Class */
// * @description Installment info
/**
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class InstallmentInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "InstallmentInfo"
     */
    public function testInstallmentInfo()
    {
    }

    /**
     * Test attribute "basketAmount"
     */
    public function testPropertyBasketAmount()
    {
    }

    /**
     * Test attribute "numberOfInstallments"
     */
    public function testPropertyNumberOfInstallments()
    {
    }

    /**
     * Test attribute "installmentAmount"
     */
    public function testPropertyInstallmentAmount()
    {
    }

    /**
     * Test attribute "firstInstallmentAmount"
     */
    public function testPropertyFirstInstallmentAmount()
    {
    }

    /**
     * Test attribute "lastInstallmentAmount"
     */
    public function testPropertyLastInstallmentAmount()
    {
    }

    /**
     * Test attribute "interestRate"
     */
    public function testPropertyInterestRate()
    {
    }

    /**
     * Test attribute "effectiveInterestRate"
     */
    public function testPropertyEffectiveInterestRate()
    {
    }

    /**
     * Test attribute "effectiveAnnualPercentageRate"
     */
    public function testPropertyEffectiveAnnualPercentageRate()
    {
    }

    /**
     * Test attribute "totalInterestAmount"
     */
    public function testPropertyTotalInterestAmount()
    {
    }

    /**
     * Test attribute "startupFee"
     */
    public function testPropertyStartupFee()
    {
    }

    /**
     * Test attribute "monthlyFee"
     */
    public function testPropertyMonthlyFee()
    {
    }

    /**
     * Test attribute "totalAmount"
     */
    public function testPropertyTotalAmount()
    {
    }

    /**
     * Test attribute "installmentProfileNumber"
     */
    public function testPropertyInstallmentProfileNumber()
    {
    }

    /**
     * Test attribute "readMore"
     */
    public function testPropertyReadMore()
    {
    }
}
