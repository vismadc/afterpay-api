<?php
/**
 * AddShippingDetailsRequestTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Visma\AfterPayApi;

/**
 * AddShippingDetailsRequestTest Class Doc Comment
 *
 * @category    Class */
// * @description Add shipping details request
/**
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AddShippingDetailsRequestTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AddShippingDetailsRequest"
     */
    public function testAddShippingDetailsRequest()
    {
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
    }

    /**
     * Test attribute "shippingCompany"
     */
    public function testPropertyShippingCompany()
    {
    }

    /**
     * Test attribute "trackingId"
     */
    public function testPropertyTrackingId()
    {
    }

    /**
     * Test attribute "trackingUrl"
     */
    public function testPropertyTrackingUrl()
    {
    }
}
