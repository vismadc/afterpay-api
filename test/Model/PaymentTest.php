<?php
/**
 * PaymentTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Visma\AfterPayApi;

/**
 * PaymentTest Class Doc Comment
 *
 * @category    Class */
// * @description Payment information
/**
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PaymentTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Payment"
     */
    public function testPayment()
    {
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
    }

    /**
     * Test attribute "contractId"
     */
    public function testPropertyContractId()
    {
    }

    /**
     * Test attribute "directDebit"
     */
    public function testPropertyDirectDebit()
    {
    }

    /**
     * Test attribute "campaign"
     */
    public function testPropertyCampaign()
    {
    }

    /**
     * Test attribute "invoice"
     */
    public function testPropertyInvoice()
    {
    }

    /**
     * Test attribute "account"
     */
    public function testPropertyAccount()
    {
    }

    /**
     * Test attribute "consolidatedInvoice"
     */
    public function testPropertyConsolidatedInvoice()
    {
    }

    /**
     * Test attribute "installment"
     */
    public function testPropertyInstallment()
    {
    }
}
