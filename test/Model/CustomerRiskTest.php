<?php
/**
 * CustomerRiskTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Visma\AfterPayApi;

/**
 * CustomerRiskTest Class Doc Comment
 *
 * @category    Class */
// * @description 
/**
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CustomerRiskTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "CustomerRisk"
     */
    public function testCustomerRisk()
    {
    }

    /**
     * Test attribute "existingCustomer"
     */
    public function testPropertyExistingCustomer()
    {
    }

    /**
     * Test attribute "verifiedCustomerIdentification"
     */
    public function testPropertyVerifiedCustomerIdentification()
    {
    }

    /**
     * Test attribute "marketingOptIn"
     */
    public function testPropertyMarketingOptIn()
    {
    }

    /**
     * Test attribute "customerSince"
     */
    public function testPropertyCustomerSince()
    {
    }

    /**
     * Test attribute "customerClassification"
     */
    public function testPropertyCustomerClassification()
    {
    }

    /**
     * Test attribute "acquisitionChannel"
     */
    public function testPropertyAcquisitionChannel()
    {
    }

    /**
     * Test attribute "hasCustomerCard"
     */
    public function testPropertyHasCustomerCard()
    {
    }

    /**
     * Test attribute "customerCardSince"
     */
    public function testPropertyCustomerCardSince()
    {
    }

    /**
     * Test attribute "customerCardClassification"
     */
    public function testPropertyCustomerCardClassification()
    {
    }

    /**
     * Test attribute "profileTrackingId"
     */
    public function testPropertyProfileTrackingId()
    {
    }

    /**
     * Test attribute "ipAddress"
     */
    public function testPropertyIpAddress()
    {
    }

    /**
     * Test attribute "numberOfTransactions"
     */
    public function testPropertyNumberOfTransactions()
    {
    }

    /**
     * Test attribute "customerIndividualScore"
     */
    public function testPropertyCustomerIndividualScore()
    {
    }

    /**
     * Test attribute "userAgent"
     */
    public function testPropertyUserAgent()
    {
    }
}
