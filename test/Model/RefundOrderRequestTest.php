<?php
/**
 * RefundOrderRequestTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Visma\AfterPayApi;

/**
 * RefundOrderRequestTest Class Doc Comment
 *
 * @category    Class */
// * @description Required when partially refunding a capture.
/**
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class RefundOrderRequestTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "RefundOrderRequest"
     */
    public function testRefundOrderRequest()
    {
    }

    /**
     * Test attribute "captureNumber"
     */
    public function testPropertyCaptureNumber()
    {
    }

    /**
     * Test attribute "items"
     */
    public function testPropertyItems()
    {
    }

    /**
     * Test attribute "refundType"
     */
    public function testPropertyRefundType()
    {
    }

    /**
     * Test attribute "parentTransactionReference"
     */
    public function testPropertyParentTransactionReference()
    {
    }

    /**
     * Test attribute "transactionReference"
     */
    public function testPropertyTransactionReference()
    {
    }

    /**
     * Test attribute "creditNoteNumber"
     */
    public function testPropertyCreditNoteNumber()
    {
    }

    /**
     * Test attribute "merchantId"
     */
    public function testPropertyMerchantId()
    {
    }
}
