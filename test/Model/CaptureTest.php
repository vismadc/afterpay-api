<?php
/**
 * CaptureTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Visma\AfterPayApi;

/**
 * CaptureTest Class Doc Comment
 *
 * @category    Class */
// * @description Capture
/**
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CaptureTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Capture"
     */
    public function testCapture()
    {
    }

    /**
     * Test attribute "reservationId"
     */
    public function testPropertyReservationId()
    {
    }

    /**
     * Test attribute "customerNumber"
     */
    public function testPropertyCustomerNumber()
    {
    }

    /**
     * Test attribute "captureNumber"
     */
    public function testPropertyCaptureNumber()
    {
    }

    /**
     * Test attribute "orderNumber"
     */
    public function testPropertyOrderNumber()
    {
    }

    /**
     * Test attribute "amount"
     */
    public function testPropertyAmount()
    {
    }

    /**
     * Test attribute "balance"
     */
    public function testPropertyBalance()
    {
    }

    /**
     * Test attribute "totalRefundedAmount"
     */
    public function testPropertyTotalRefundedAmount()
    {
    }

    /**
     * Test attribute "currency"
     */
    public function testPropertyCurrency()
    {
    }

    /**
     * Test attribute "insertedAt"
     */
    public function testPropertyInsertedAt()
    {
    }

    /**
     * Test attribute "updatedAt"
     */
    public function testPropertyUpdatedAt()
    {
    }

    /**
     * Test attribute "directDebitBankAccount"
     */
    public function testPropertyDirectDebitBankAccount()
    {
    }

    /**
     * Test attribute "directDebitSwift"
     */
    public function testPropertyDirectDebitSwift()
    {
    }

    /**
     * Test attribute "accountProfileNumber"
     */
    public function testPropertyAccountProfileNumber()
    {
    }

    /**
     * Test attribute "numberOfInstallments"
     */
    public function testPropertyNumberOfInstallments()
    {
    }

    /**
     * Test attribute "installmentAmount"
     */
    public function testPropertyInstallmentAmount()
    {
    }

    /**
     * Test attribute "contractDate"
     */
    public function testPropertyContractDate()
    {
    }

    /**
     * Test attribute "orderDate"
     */
    public function testPropertyOrderDate()
    {
    }

    /**
     * Test attribute "installmentProfileNumber"
     */
    public function testPropertyInstallmentProfileNumber()
    {
    }

    /**
     * Test attribute "parentTransactionReference"
     */
    public function testPropertyParentTransactionReference()
    {
    }

    /**
     * Test attribute "transactionReference"
     */
    public function testPropertyTransactionReference()
    {
    }

    /**
     * Test attribute "dueDate"
     */
    public function testPropertyDueDate()
    {
    }

    /**
     * Test attribute "invoiceDate"
     */
    public function testPropertyInvoiceDate()
    {
    }

    /**
     * Test attribute "yourReference"
     */
    public function testPropertyYourReference()
    {
    }

    /**
     * Test attribute "ourReference"
     */
    public function testPropertyOurReference()
    {
    }

    /**
     * Test attribute "invoiceProfileNumber"
     */
    public function testPropertyInvoiceProfileNumber()
    {
    }

    /**
     * Test attribute "ocr"
     */
    public function testPropertyOcr()
    {
    }

    /**
     * Test attribute "installmentCustomerInterestRate"
     */
    public function testPropertyInstallmentCustomerInterestRate()
    {
    }

    /**
     * Test attribute "imageUrl"
     */
    public function testPropertyImageUrl()
    {
    }

    /**
     * Test attribute "captureItems"
     */
    public function testPropertyCaptureItems()
    {
    }

    /**
     * Test attribute "shippingDetails"
     */
    public function testPropertyShippingDetails()
    {
    }
}
