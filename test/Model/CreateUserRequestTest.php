<?php
/**
 * CreateUserRequestTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Visma\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Visma\AfterPayApi;

/**
 * CreateUserRequestTest Class Doc Comment
 *
 * @category    Class */
// * @description Create user for customer account request
/**
 * @package     Visma\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CreateUserRequestTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "CreateUserRequest"
     */
    public function testCreateUserRequest()
    {
    }

    /**
     * Test attribute "companyId"
     */
    public function testPropertyCompanyId()
    {
    }

    /**
     * Test attribute "customer"
     */
    public function testPropertyCustomer()
    {
    }

    /**
     * Test attribute "limit"
     */
    public function testPropertyLimit()
    {
    }

    /**
     * Test attribute "nfcId"
     */
    public function testPropertyNfcId()
    {
    }
}
